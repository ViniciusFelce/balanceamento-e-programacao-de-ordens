let htmlPage = `
<!DOCTYPE html> 
<html lang="pt-br"> 
<head> 
    <meta charset="UTF-8"> 
    <meta http-equiv="Pragma" content="no-cache" /> 
    <meta http-equiv="Expires" content="-1" /> 
    <meta http-equiv="Cache-Control" content="no-cache" /> 
    <title>Calendário</title> 
</head> 
<style> 

p {
  margin: 0;
}

.tituloMes {
  font-size: 18px;
    margin-bottom: 8px;
  padding: 5px 5px 15px 5px;
}

#tituloContainer {
  display: flex;
  flex-direction: row;
  align-items: center;
    margin-left: 60px;
}

.dayContainer {
  width: 55px;
  height: auto;
}

.numberContainer {
  display: flex;
  justify-content: center;
  border-width: 0px;
  border-color: black;
  border-style: solid;
  background-color: #c0bfbd;
  height: 18px;
  width: 60px;
}

.mesLinhasContainer {
  display: flex;
  margin-left: 50px;
}

.mesLinha {
  margin-top: 10px;
  margin-bottom: 10px;
}

.areaContainer {
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 112%;
  background-color: beige;
  padding-top: 10px;
  padding-bottom: 20px;
  border-left: 1px solid #00000032;
}

.osContainer {
  width: 95%;
  height: fit-content;
  display: flex;
  justify-content: center;
  background-color: #93d358;
  border-width: 1px;
  border-color: #73a744;
  border-style: solid;
  margin-top: 2px;
}

.btnAvancar,
.btnRetornar {
  font-size: 20px;
  padding: 10px;
  margin-bottom: 13px;
  border: none; 
  border-radius: 5px;   
}

.btnRetornar:hover,
.btnAvancar:hover {
  cursor: pointer;
  background-color: #d8d8d8;
}

.departAndOs {
  display: flex;
}

.titleDiv {
  font-weight: bold;
}

.maoDeObraDiv {
  margin-top: 25px;
  width: 420px;
  background-color: #7cc99c;
}

.titleDepartOs {
  background-color: #08b0f1;
  color: black;
  height: 22px;
  font-weight: bold;
  width: 100px;
  padding: 2px;
  border: 1px solid black;
}

.dropbtn {
  display: flex;
  background-color: white;
  font-size: 13px;
  animation: none;
  height: 22px;
  width: 100px;
  margin-right: 20px;
}

.funcTitle {
  background-color: #08b0f1;
  color: black;
  font-weight: bold;
  border: 1px solid black;
  width: 420px;
  text-align: center;
  margin-top: 10px;
  padding: 2px;
}

.dropdown,
.dropdown-os {
  position: relative;
  display: inline-block;
}

.descOsMain {
  display: inline-flex;
}

.descDiv {
  display: flex;
  font-size: 15px;
  height: 25px;
  align-items: center;
  justify-content: center;
}

.descTitle {
  display: flex;
  font-size: 15px;
  height: 25px;
  align-items: center;
  justify-content: center;
  font-weight: bold;
  font-size: 17px;
}

.ordemOs {
  width: 85px;
  text-align: center;
}

/* .descOs {
  width: 90px;
  text-align: center;
} */

.descOsMainTitle {
  display: inline-flex;    
}

.containerDescOsMain {
  display: inline-flex;
  margin-top: 50px;
}

.containerDescOs {
  display: inline-flex;
  flex-direction: column;
}

.descOsMainDefault{
  display: inline-flex;
  flex-direction: row;
}

.descOs {
  width: 110px;
  text-align: center;
  border-left: 1px solid #00000032;
  border-bottom: 1px solid #00000032;
}

.descOsDefault {
  width: 110px;
  text-align: center;   
}

.descOsMp {
  width: 172px;
  text-align: center;
  border-left: 1px solid #00000032;
  border-bottom: 1px solid #00000032;
}

.descOsName {
  width: 190px;
  text-align: center;
  border-right: 1px solid #00000032;
  border-left: 1px solid #00000032;
  border-bottom: 1px solid #00000032;
}

.tipoOs {
  width: 80px;
  text-align: center;
}

.mpOs {
  width: 100px;
  text-align: center;
}

.hhmpOs {
  width: 90px;
  text-align: center;
}

.tMinOs {
  width: 80px;
  text-align: center;
}

.progOs {
  width: 90px;
  text-align: center;
}

.tMaxOs {
  width: 70px;
  text-align: center;
}

.maoObraOs {
  width: 130px;
  text-align: center;
}

.usoDoRecursoDiv {
  margin-top: 25px;
  background-color: #7cc99c;
}

.detalhesOSDiv {
  margin-left: 50px;
  width: 1353px;
  display: flex;
  margin-top: 25px;
  flex-direction: column;
}

.detalhesOSTitle {
  background-color: #7cc99c;
}

.mesTopClass {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 200px;
}

.dropdown-content,
.dropdown-content-tipo {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 150px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  padding: 12px 16px;
  z-index: 1;
  max-height: 240px;
  overflow-y: auto;
}

.dropdown-content a,
.dropdown-content-tipo a {
  display: block;
  text-decoration: none;
  color: black;
  margin-bottom: 10px;
}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdown-os:hover .dropdown-content-tipo {
  display: block;
}

.funcUsoDoRecurso {
  display: block;
  margin: 0 auto;
}

.funcTitleUsoDoRecurso {
  background-color: #08b0f1;
  color: black;
  border: 1px solid black;
  font-weight: bold;
  width: 508px;
  text-align: center;
  padding: 2px;
}

.maoDeObraRecursoContainer {
  display: flex;
  flex-direction: row;
  margin-left: 50px;
  margin-top: 20px;
}

.mesLegendaContainer {
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  margin-left: 50px;
}

.legVerdeDiv {
  background-color: #93d358;
  height: 15px;
  width: 40px;
  margin-right: 5px;
  margin-top: 1px;
}

.legAmareloDiv {
  background-color: #FFBF2E;
  height: 15px;
  width: 40px;
  margin-right: 5px;
  margin-top: 1px;
}

.legendasDiv {
  display: flex;
  flex-direction: column;
  justify-content: left;
  align-content: end;
}

.funcDetails {
  display: flex;
  flex-direction: column;
}

.funcDetailsTitle {
  display: flex;
  flex-direction: row;
}

.funcDetailsData {
  display: flex;
  flex-direction: row;
}

.detailsTitle {
  font-weight: bold;
  border: 1px solid black;
  width: 105px;
  text-align: center;
  padding: 2px;
}

.detailsData {
  border: 1px solid black;
  width: 105px;
  text-align: center;
  padding: 2px;
}

.usoDoRecursoTitleDiv {
  display: flex;
  flex-direction: row;
}

.usoDoRecursoTitle {
  font-weight: bold;
  border: 1px solid black;
  width: 127px;
  text-align: center;
  padding: 2px;
}

.usoDoRecursoDataDiv {
  display: flex;
  flex-direction: row;
}

.usoDoRecursoData {
  border: 1px solid black;
  width: 127px;
  text-align: center;
  padding: 2px;
}

.mesUsoDoRecurso {
  background-color: #08b0f1;
  border: 1px solid black;
  color: black;
  font-weight: bold;
  width: 57px;
  text-align: center;
  padding: 2px;
}

.ordensUsoDoRecurso {
  background-color: #08b0f1;
  border: 1px solid black;
  color: black;
  font-weight: bold;
  width: 336px;
  text-align: center;
  padding: 2px;
}

.hhDispUsoDoRecurso {
  background-color: #08b0f1;
  border: 1px solid black;
  color: black;
  font-weight: bold;
  width: 115px;
  text-align: center;
  padding: 2px;
}

.osDia {
  border: 1px solid black;
  color: black;
  width: 57px;
  text-align: center;
  padding: 2px;
}

.osNumero {
  border: 1px solid black;
  color: black;
  width: 112px;
  text-align: center;
  padding: 2px;
}

.osHhDisp {
  border: 1px solid black;
  color: black;
  width: 115px;
  text-align: center;
  padding: 2px;
}

.usoDoRecursoDetails {
  display: inline-flex;
}

</style>

  <body>
    <section class="mesContainer">
      <div id="tituloContainer">
            <button id="mesAnterior" class="btnRetornar"><svg xmlns="http://www.w3.org/2000/svg" class="botaoSVG" height="24px" viewBox="0 -960 960 960" width="24px" fill="#383838"><path class="botaoSVG" d="M400-80 0-480l400-400 71 71-329 329 329 329-71 71Z"/></svg></button>
        <div style="width: 180px; text-align: center;" id="mesNomeContainer"></div>
            <button id="mesPosterior" class="btnAvancar"><svg xmlns="http://www.w3.org/2000/svg" class="botaoSVG" height="24px" viewBox="0 -960 960 960" width="24px" fill="#383838"><path class="botaoSVG" d="m321-80-71-71 329-329-329-329 71-71 400 400L321-80Z"/></svg></button>            
            <!--
            div class="undoContainer">
                <button class="undoBtnStyle" id="undoBtn"><svg xmlns="http://www.w3.org/2000/svg" height="27px" viewBox="0 -960 960 960" width="27px" fill="#383838"><path d="M280-200v-80h284q63 0 109.5-40T720-420q0-60-46.5-100T564-560H312l104 104-56 56-200-200 200-200 56 56-104 104h252q97 0 166.5 63T800-420q0 94-69.5 157T564-200H280Z"/></svg></button>
            </div>
            -->
      </div>
      <div style="display: inline-flex"></div>
      <div class="contDetailsOs">
        <div></div>
      </div>
    </section>
    <div class="mesTopClass">
      <span id="mesTop"></span>
    </div>
    <section class="mesLegendaContainer">
      <div class="legendasDiv">
        <div style="display: flex; flex-direction: row">
          <div class="legVerdeDiv">&nbsp;&nbsp;</div>
          <div>OS com mão de obra alocada</div>
        </div>
        <div style="display: flex; flex-direction: row">
          <div class="legAmareloDiv"></div>
          <div>OS sem mão de obra alocada</div>
        </div>
      </div>
    </section>
    <section class="mesLinhasContainer">
      <section class="mesLinha mesLinha1" style="display: flex; flex-direction: row"></section>
      <section class="mesLinha mesLinha2" style="display: flex; flex-direction: row"></section>
    </section>
    <section class="maoDeObraRecursoContainer">
      <div>
        <div class="maoDeObraDiv">
          <p style="margin-left: 120px; color: white; font-size: large; padding: 3px;">
            Detalhes da mão de Obra
          </p>
        </div>
        <div class="departAndOs">
          <div>
            <p class="titleDepartOs">Departamento</p>
          </div>
          <div class="dropdown">
            <button id="dropdown-btn" class="dropbtn">Selecionar&nbsp;&nbsp;<svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 -960 960 960" width="20px" fill="#000000">
                <path d="M480-344 240-584l43-43 197 197 197-197 43 43-240 240Z" />
              </svg>
            </button>
            <div class="dropdown-content" id="dropdown-content-id">
              <a href="#" class="menu">DepartamentoTEST</a>
            </div>
          </div>
          <div>
            <p class="titleDepartOs">Tipo da OS</p>
          </div>
          <div class="dropdown-os">
            <button id="dropdown-btn-os" class="dropbtn"> Selecionar&nbsp;&nbsp;<svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 -960 960 960" width="20px" fill="#000000">
                <path d="M480-344 240-584l43-43 197 197 197-197 43 43-240 240Z" />
              </svg>
            </button>
            <div id="dropdown-content-tipo" class="dropdown-content-tipo"></div>
          </div>
        </div>
        <div>
          <p class="funcTitle">Funcionário</p>
        </div>
        <div class="funcDetails">
          <div class="funcDetailsTitle">
            <p class="detailsTitle">Nome</p>
            <p class="detailsTitle">Matrícula</p>
            <p class="detailsTitle">Área</p>
            <p class="detailsTitle">Nível</p>
          </div>
          <div id="func1" class="funcDetailsData">
            <span class="detailsData">José Moraes</span>
            <span class="detailsData">150186</span>
            <span class="detailsData">O&M</span>
            <span class="detailsData">II</span>
          </div>
          <div id="func2" class="funcDetailsData">
            <span class="detailsData">Bruno Prado</span>
            <span class="detailsData">150185</span>
            <span class="detailsData">Faixa</span>
            <span class="detailsData">III</span>
          </div>
        </div>
      </div>
      <div>
        <div class="usoDoRecursoDiv">
          <p style="text-align: center; color: white; font-size: large; padding: 3px;">
            Uso do Recurso
          </p>
        </div>
        <div class="funcUsoDoRecurso">
          <div>
            <p class="funcTitleUsoDoRecurso">Funcionário</p>
          </div>
          <div class="usoDoRecursoTitleDiv">
            <p class="usoDoRecursoTitle">Nome</p>
            <p class="usoDoRecursoTitle">Matrícula</p>
            <p class="usoDoRecursoTitle">Área</p>
            <p class="usoDoRecursoTitle">Nível</p>
          </div>
          <div id="recurso" class="usoDoRecursoDataDiv">
            <span class="usoDoRecursoData">José Moraes</span>
            <span class="usoDoRecursoData">150186</span>
            <span class="usoDoRecursoData">O&M</span>
            <span class="usoDoRecursoData">II</span>
          </div>
          <div class="usoDoRecursoDetails">
            <div class="mesUsoDoRecurso">
              <span>jan/24</span>
            </div>
            <div class="ordensUsoDoRecurso">
              <span>Ordens</span>
            </div>
            <div class="hhDispUsoDoRecurso">
              <span>Hh disponível</span>
            </div>
          </div>
          <div style="display: flex; flex-direction: column;">
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>1</span>
              </div>
              <div class="osNumero">
                <span>203212</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span>0h</span>
              </div>
            </div>
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>2</span>
              </div>
              <div class="osNumero">
                <span>403513</span>
              </div>
              <div class="osNumero">
                <span>231512</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span>0h</span>
              </div>
            </div>
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>3</span>
              </div>
              <div class="osNumero">
                <span>403515</span>
              </div>
              <div class="osNumero">
                <span>231514</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span>1h</span>
              </div>
            </div>
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>4</span>
              </div>
              <div class="osNumero">
                <span>403522</span>
              </div>
              <div class="osNumero">
                <span>403526</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span>0h</span>
              </div>
            </div>
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>5</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span> </span>
              </div>
            </div>
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>6</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span> </span>
              </div>
            </div>
            <div class="usoDoRecursoDetails">
              <div class="osDia">
                <span>7</span>
              </div>
              <div class="osNumero">
                <span>203312</span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osNumero">
                <span> </span>
              </div>
              <div class="osHhDisp">
                <span>1h</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="detalhesOSDiv">
        <div class="detalhesOSTitle">
          <p style="text-align: center; color: white; font-size: large; padding: 3px;">
            Detalhes da Ordem de Serviço
          </p>
        </div>
        <div class="containerDescOs">
          <div class="descOsMainTitle">
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Ordem</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Atividade</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Descrição</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Tipo</p>
              </div>                
            </div>
            <div class="descOsMp">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">MP</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">HH MP</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">T.Min</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Prog</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">T.Max</p>
              </div>                
            </div>
            <div class="descOs">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Mão de Obra</p>
              </div>                
            </div>
            <div class="descOsName">
              <div class="descDiv" style="background-color: #bfbfbf;">
                <p class="descTitle">Nome</p>
              </div>                
            </div>
          </div>
          <div id='htmlPageDescDiv' style="display: flex; flex-direction: column">
            <div class="descOsMainDefault">
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOsMp">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOs">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
              <div class="descOsName">        
                <div class="descDiv">
                  <span class="spanInfo">-</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>`;

let listaOsDesc;

function buscaInfoOs(id) {
	const dados = id.split('-');
	const osNumber = dados[0];
	const atividadeNumber = dados[1];
	const idProg = dados[2];
	const osSelecionada = listaOsDesc.filter((os) => os.os == osNumber);
	const listaAtividades = osSelecionada[0].atividades;
	const prog = listaAtividades.filter(
		(ativ) => ativ.atividade == atividadeNumber && ativ.id == idProg
	);
	prog.os = osNumber;
	return prog;
}

function gridDepart() {
  response = EAM.Ajax.request({
    url: "GRIDDATA",
    params: {
      GRID_NAME: "1UDPRT",
      REQUEST_TYPE: "LIST.HEAD_DATA.STORED",
      LOV_ALIAS_NAME_1: "code",
      LOV_ALIAS_VALUE_1: 1,
      LOV_ALIAS_TYPE_1: "",
    },
    async: false,
    method: "POST",
  });
  var gridresult = response.responseData.pageData.grid.GRIDRESULT.GRID;
  return gridresult;
}

function gridTipoOS() {
  responseTPOS = EAM.Ajax.request({
    url: "GRIDDATA",
    params: {
      GRID_NAME: "1UTPOS",
      REQUEST_TYPE: "LIST.HEAD_DATA.STORED",
      LOV_ALIAS_NAME_1: "",
      LOV_ALIAS_VALUE_1: "",
      LOV_ALIAS_TYPE_1: "",
    },
    async: false,
    method: "POST",
  });
  var gridresultTipo = responseTPOS.responseData.pageData.grid.GRIDRESULT.GRID;
  return gridresultTipo;
}

function gridOS(dataInicio, dataFim) {
    let listaOS = [];
    let numeroLinhas = 100;
    let cursor = 1;

    for (let index = 0; index < 2000; index++) {
  responseOS = EAM.Ajax.request({
    url: "GRIDDATA",
    params: {
      GRID_NAME: "1UORDE",
      REQUEST_TYPE: "LIST.HEAD_DATA.STORED",
                NUMBER_OF_ROWS_FIRST_RETURNED: numeroLinhas,
                CURSOR_POSITION: cursor,
                LOV_ALIAS_NAME_1: "dataInicio",
                LOV_ALIAS_VALUE_1: dataInicio,
      LOV_ALIAS_TYPE_1: "",
                LOV_ALIAS_NAME_2: "dataFim",
                LOV_ALIAS_VALUE_2: dataFim,
                LOV_ALIAS_TYPE_2: "",
    },
    async: false,
    method: "POST",
  });
        var gridresultOS =
            responseOS.responseData.pageData.grid.GRIDRESULT.GRID.DATA;

        if (gridresultOS.length == 0) {
            break;
        }

        cursor = cursor + numeroLinhas;
        let listaNova = listaOS.concat(gridresultOS);
        listaOS = listaNova;
    }
    let listaOsFormatada = formatandoGridOs(listaOS);

    return listaOsFormatada;
}

function formatandoGridOs(requisicao) {
    let json = [];
    let id = 1;

    for (let index = 0; index < requisicao.length; index++) {
        if (
            requisicao[index].acs_sched != '' &&
            requisicao[index].acs_sched != null &&
            requisicao[index].acs_sched != undefined &&
            requisicao[index].acs_hours != '' &&
            requisicao[index].acs_hours != null &&
            requisicao[index].acs_hours != undefined &&
            requisicao[index].evt_type != '' &&
            requisicao[index].evt_type != null &&
            requisicao[index].evt_type != undefined &&
            requisicao[index].evt_target != '' &&
            requisicao[index].evt_target != null &&
            requisicao[index].evt_target != undefined &&
            requisicao[index].evt_requeststart != '' &&
            requisicao[index].evt_requeststart != null &&
            requisicao[index].evt_requeststart != undefined &&
            requisicao[index].evt_requestend != '' &&
            requisicao[index].evt_requestend != null &&
            requisicao[index].evt_requestend != undefined 
        ) {                
            let detalhes = {
                id: id,
                atividade: requisicao[index].act_act,
                dataInicioProgr: new Date(
                    Number(requisicao[index].evt_target.substring(6, 10)),
                    Number(requisicao[index].evt_target.substring(3, 5) - 1),
                    Number(requisicao[index].evt_target.substring(0, 2))
                ),
                tipoOS: requisicao[index].evt_jobtype,
                descricaoTipoOs: requisicao[index].evt_type,
                maoDeObra: requisicao[index].acs_responsible,
                departamento: requisicao[index].evt_mrc,
                mp: requisicao[index].evt_ppm,
                descricaoOS: requisicao[index].evt_note,
                toleranciaMin: new Date(
                    Number(requisicao[index].evt_requeststart.substring(6, 10)),
                    Number(requisicao[index].evt_requeststart.substring(3, 5) - 1),
                    Number(requisicao[index].evt_requeststart.substring(0, 2))
                ),
                toleranciaMax: new Date(
                    Number(requisicao[index].evt_requestend.substring(6, 10)),
                    Number(requisicao[index].evt_requestend.substring(3, 5) - 1),
                    Number(requisicao[index].evt_requestend.substring(0, 2))
                ),
                mmhp: requisicao[index].acs_hours,
                funcionario: requisicao[index].acsresponsibledesc,
                dataProgramacao: new Date(
                    Number(requisicao[index].acs_sched.substring(6, 10)),
                    Number(requisicao[index].acs_sched.substring(3, 5) - 1),
                    Number(requisicao[index].acs_sched.substring(0, 2))
                )
            };
            let osExistente;
            for (let i = 0; i < json.length; i++) {
                if (json[i].os == requisicao[index].evt_code) {
                    osExistente = i;
                }
            }
            if (osExistente != undefined) {
                id++;
                json[osExistente].atividades.push(detalhes);
            } else {
                id++;
                let objeto = {
                    os: requisicao[index].evt_code,
                    atividades: [],
                };
                objeto.atividades.push(detalhes);
                json.push(objeto);
            }
        }
    }
    return json;
}

function eventoClickMenu() {
  let listaOpcoesMenu = document.getElementsByClassName("menu");

  for (let index = 0; index < listaOpcoesMenu.length; index++) {
    listaOpcoesMenu[index].addEventListener("click", function () {
      let os = listaOpcoesMenu[index].innerHTML;
      selectDepartment(depart);
    },
    false
    );
  }
}

function selectDepartment(department) {
  let menuBotao = document.getElementById("dropdown-btn");
  menuBotao.textContent = department;
}

function eventoClickMenuOs() {
  let listaOpcoesMenuOs = document.getElementsByClassName("menu-os");

  for (let index = 0; index < listaOpcoesMenuOs.length; index++) {
    listaOpcoesMenuOs[index].addEventListener("click", function () {
      let os = listaOpcoesMenuOs[index].innerHTML;
      selectOs(os);
    },
    false
    );
  }
}

function selectOs(Os) {
  let menuBotaoOs = document.getElementById("dropdown-btn-os");
  menuBotaoOs.textContent = Os;
}

let dadosMes;
let mesFormatado;

function configuracoesDeTela() {
  try {
    Ext.ComponentQuery.query("uxtabpanel")[0].el.dom.style.height = "0px";
    var referencenode = Ext.ComponentQuery.query("uxtabpanel")[0].up().body.dom;
    vWidth = referencenode.clientWidth;
    vHeigh = referencenode.clientHeight;
    Ext.ComponentQuery.query("[uftId=resetrec]")[0].enable();
    Ext.ComponentQuery.query("[uftId=enterdesigner]")[0].disable();
    Ext.ComponentQuery.query("[uftId=epak]")[0].disable();
    Ext.ComponentQuery.query("[uftId=help]")[0].disable();
  } catch (err) {
    console.log(err);
  }

  // remove custom-div
  var vCustomDiv = document.getElementById("custom-div");
  if (vCustomDiv) {
    vCustomDiv.parentElement.removeChild(vCustomDiv);
  }

  // create custom-div
  var node = document.createElement("div");
  node.id = "custom-div";
  node.style.width = "100%";
  node.style.height = "100%";
  referencenode.appendChild(node);

  //add html page
  var divcustom = document.getElementById("custom-div");
  divcustom.innerHTML = htmlPage;
}

function gerarDadosMes(valor = null) {
  let dataAtual = new Date();
  let mes;
  let ano;

  if (valor == null) {
    mes = dataAtual.getMonth() + 1;
    ano = dataAtual.getFullYear();
  }

  if (valor == "posterior") {
    if (dadosMes.mes == 12) {
      mes = 1;
      ano = dadosMes.ano + 1;
    } else {
      mes = dadosMes.mes + 1;
      ano = dadosMes.ano;
    }
  }

  if (valor == "anterior") {
    if (dadosMes.mes == 1) {
      mes = 12;
      ano = dadosMes.ano - 1;
    } else {
      mes = dadosMes.mes - 1;
      ano = dadosMes.ano;
    }
  }

  let data = new Date(ano, mes, 0);
  let diasNoMes = data.getDate();

  let mesString =
    mes == 1
      ? "Janeiro"
      : mes == 2
      ? "Fevereiro"
      : mes == 3
      ? "Março"
      : mes == 4
      ? "Abril"
      : mes == 5
      ? "Maio"
      : mes == 6
      ? "Junho"
      : mes == 7
      ? "Julho"
      : mes == 8
      ? "Agosto"
      : mes == 9
      ? "Setembro"
      : mes == 10
      ? "Outubro"
      : mes == 11
      ? "Novembro"
      : mes == 12
      ? "Dezembro"
      : "error";

  dadosMes = {
    dias: diasNoMes,
    mes: mes,
    ano: ano,
  };

  mesFormatado = formataMes(mes);

  let mesNomeContainer = document.getElementById("mesNomeContainer");
    mesNomeContainer.innerHTML = `<h1 class="tituloMes" style="font-size: 21px">${mesString} / ${ano}</h1>`;

  let mesNomeTop = document.getElementById("mesTop");
  mesNomeTop.innerHTML = `<p class= tituloMesTop>${mesString}/${ano}</p>`;

    let dataInicio = `${mes.toString().length == 1 ? "0" + mes.toString() : mes.toString()}-01-${ano}`;
    let dataFim = `${mes.toString().length == 1 ? "0" + mes.toString() : mes.toString()}-${diasNoMes}-${ano}`;
    listaOsDesc = gridOS(dataInicio, dataFim);
    console.log("listaOsDesc: ", listaOsDesc);
}

function verificaEventosBotaoTrocaMes() {
  document.getElementById("mesAnterior").addEventListener("click", function () {
    gerarDadosMes("anterior");
    geraCalendarioMes();
    geraOS();
    eventoClickDescOs();
  });
  document
    .getElementById("mesPosterior")
    .addEventListener("click", function () {
      gerarDadosMes("posterior");
      geraCalendarioMes();
      geraOS();
      eventoClickDescOs();
    });
}

function geraCalendarioMes() {
  var sectionMes = document.getElementsByClassName("mesLinha")[0];
  sectionMes.innerHTML = "<div></div>";

  let mesLinha = [];

  for (let dia = 1; dia < dadosMes.dias + 1; dia++) {
    let diaFormatado = formataDia(dia);
    var diaComponent = [
      `<div class="dayContainer">`,
      `    <div style="display: flex; justify-content: center; align-text: center" id="divHorasTotaisOsDia-${diaFormatado}">`,
      `    <span class="" id="horasTotaisOsDia-${diaFormatado}">0</span><span>h</span>`,
      `    </div>`,
      `    <div class="numberContainer">`,
      `        <p>${dia}</p>`,
      `    </div>`,
      `    <div class="areaContainer" id="${diaFormatado}-${mesFormatado}-${dadosMes.ano}">`,
      `    </div>`,
      `</div>`,
    ].join("");

    if (dia <= 31) {
      mesLinha.push(diaComponent);
    }
  }
  let mesHtml = mesLinha.join("");
  sectionMes.innerHTML = mesHtml;
}

let osInfos = pegarOS();

function geraOS() {
  let areaDia;
    for (let iOs = 0; iOs < listaOsDesc.length; iOs++) {
        for (let index = 0; index < listaOsDesc[iOs].atividades.length; index++) {
            let dataFormatada = `${formataDia(listaOsDesc[iOs].atividades[index].dataProgramacao.getDate())}-${formataMes(listaOsDesc[iOs].atividades[index].dataProgramacao.getMonth() + 1)}-${listaOsDesc[iOs].atividades[index].dataProgramacao.getFullYear()}`
            let totalDataHora = 0;
            for (let num = 0; num < listaOsDesc[iOs].atividades[index].mmhp.length; num++) {
                totalDataHora += Number(listaOsDesc[iOs].atividades[index].mmhp[num]);
            }            

            areaDia = document.getElementById(dataFormatada);
            console.log('dataFormatada',dataFormatada)
            console.log('areaDia',areaDia)
            
      let osExistente = areaDia.innerHTML;
      console.log('osExistente',osExistente)
      var os = [
        `<div `,
        `    id="${listaOsDesc[iOs].os}-${listaOsDesc[iOs].atividades[index].atividade}-${listaOsDesc[iOs].atividades[index].id}"`, //
        `    class="osContainer"`,
        `    data-horas="${totalDataHora}"`,
        `    data-dia="${dataFormatada}}"`,
        `>`,
        `    ${listaOsDesc[iOs].os}`,
        `</div>`,
      ].join("");
      areaDia.innerHTML = osExistente + os;
    }
  }
}

function formataMes(mes) {
  let mesString = mes.toString();
  let mesFormatado = mesString.length == 1 ? `0${mesString}` : mesString;
  return mesFormatado;
}

function formataDia(dia) {
  let diaString = dia.toString();
  let diaFormatado = diaString.length == 1 ? `0${diaString}` : diaString;
  return diaFormatado;
}

function pegarDepartamentos() {
  const depart = gridDepart();
  const divDepartamentos = document.getElementById("dropdown-content-id");
  divDepartamentos.innerHTML = "";
  depart.DATA.forEach((item) => {
    const addTag = document.createElement("a");
    addTag.setAttribute("href", "#");
    addTag.setAttribute("class", "menu");
    addTag.textContent = item.mrc_code;
    divDepartamentos.appendChild(addTag);
  });
  eventoClickMenu();
}

function pegarTipo() {
  const Tipo = gridTipoOS();
  const divTipoOS = document.getElementById("dropdown-content-tipo");
  divTipoOS.innerHTML = "";
  Tipo.DATA.forEach((item) => {
    const addTag = document.createElement("a");
    addTag.setAttribute("href", "#");
    addTag.setAttribute("class", "menu-os");
    let osNomeBruto = item.uco_desc;
    let osNomeCorreto = osNomeBruto.slice(11);
    addTag.textContent = osNomeCorreto[0].toUpperCase() + osNomeCorreto.substring(1);
    divTipoOS.appendChild(addTag);
  });
  eventoClickMenuOs();
}

function pegarOS() {
  const os = gridOS();
  let listaDeOS = [];
  for (let index = 0; index < os.length; index++) {
    let numeroAleatiorio = Math.floor(Math.random() * 10);
    let numeroAleatiorioSemZero = numeroAleatiorio == 0 ? 1 : numeroAleatiorio;
    listaDeOS.push({
      os: os[index].evt_code,
      horas: numeroAleatiorioSemZero,
      data: new Date("2024", numeroAleatiorioSemZero % 2 == 0 ? "5" : "6", numeroAleatiorioSemZero.toString()),
    });
  }
  return listaDeOS;
}

function eventoClickDescOs() {
  const divs = document.querySelectorAll(".osContainer");
  divs.forEach((div) => {
      div.addEventListener("click", function (event) {            
          const divId = event.target.id;
          const atividadeSelecionada = buscaInfoOs(divId)
          const divHtml = document.getElementById("htmlPageDescDiv");
          let atividadeNum = atividadeSelecionada[0].atividade;
          atividadeSemZero = Number(atividadeNum.slice(0 , -1));

    linhaHtml = `
      <div class="hyperDescOs" style="display: flex">
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].os}</span>
        </div>
      </div>
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].atividade}</span>
        </div>
      </div>
      <div class="descOs">
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].descricaoOS == undefined ? '-' : atividadeSelecionada[0].descricaoOS}</span>
        </div>
      </div>
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].descricaoTipoOs}</span>
        </div>
      </div>
      <div class="descOsMp">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].mp}</span>
        </div>
      </div>
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].mmhp}</span>
        </div>
      </div>
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${formataDia(atividadeSelecionada[0].toleranciaMin.getDate())}/${formataMes(atividadeSelecionada[0].toleranciaMin.getMonth() + 1)}/${atividadeSelecionada[0].toleranciaMin.getFullYear()}</span>
        </div>
      </div>
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${formataDia(atividadeSelecionada[0].dataProgramacao.getDate())}/${formataMes(atividadeSelecionada[0].dataProgramacao.getMonth() + 1)}/${atividadeSelecionada[0].dataProgramacao.getFullYear()}</span>
        </div>
      </div>
      <div class="descOs">
        <div class="descDiv">
          <span class="spanInfo">${formataDia(atividadeSelecionada[0].toleranciaMax.getDate())}/${formataMes(atividadeSelecionada[0].toleranciaMax.getMonth() + 1)}/${atividadeSelecionada[0].toleranciaMax.getFullYear()}</span>
        </div>
      </div>
      <div class="descOs">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].maoDeObra}</span>
        </div>
      </div>
      <div class="descOsName">        
        <div class="descDiv">
          <span class="spanInfo">${atividadeSelecionada[0].funcionario}</span>
        </div>
      </div>
      </div>`; 
      divHtml.innerHTML = linhaHtml;
  });
});
}

function barraRolagem() {
    const customDiv = document.getElementById("custom-div");
    if (customDiv) {
        customDiv.style.overflow = "auto";
        customDiv.style.height = "98%";
  }
}

Ext.define("EAM.custom.external_devprg", {
  extend: "EAM.custom.AbstractExtensibleFramework",
  getSelectors: function () {
    return {
      "[extensibleFramework] [tabName=HDR][isTabView=true]": {
        afterRender: function () {
                    console.log("Teste: afterrender");
        },
        afterloaddata: function () {
                    console.log("Teste: afterload");
        },
        afterlayout: function () {
          try {
            if (
              EAM.Utils.getScreen().userFunction == "DEVPRG" &&
              Ext.ComponentQuery.query("uxtabpanel")[0]
            ) {
              configuracoesDeTela();
              verificaEventosBotaoTrocaMes();
              gerarDadosMes();
              eventoClickMenu();
              eventoClickMenuOs();
              pegarDepartamentos();
              geraCalendarioMes();
              geraOS();
              pegarDepartamentos();
              pegarTipo();
              pegarOS();
              eventoClickDescOs();
              barraRolagem();
            }
          } catch (err) {
            console.log(err);
          }
        },
      },
    };
  },
});
