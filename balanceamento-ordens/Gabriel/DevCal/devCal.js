let htmlPage = `
<!DOCTYPE html> 
<html lang="pt-br"> 
<head> 
    <meta charset="UTF-8"> 
    <meta http-equiv="Pragma" content="no-cache" /> 
    <meta http-equiv="Expires" content="-1" /> 
    <meta http-equiv="Cache-Control" content="no-cache" /> 
    <title>Calendário</title> 
</head> 
<style>

p {
    margin: 0;
}

.tituloMes {
    font-size: 18px;
    margin-bottom: 8px;
    padding: 5px 5px 15px 5px;
}

#tituloContainer {
    display: flex;
    flex-direction: row;
    align-items: center;    
    margin-left: 60px;
}

.dayContainer,
.monthContainer {
    width: 55px;
    height: auto;
}

.mesNomeContainer{
    display: flex;
    width: 180px;
    justify-content: center;
}


.numberContainer {
    display: flex;
    justify-content: center;
    border-width: 0px;
    border-color: black;
    border-style: solid;
    background-color: #c0bfbd;
    font-weight: bold;
    height: 18px;
    width: 56px;
}

.numberContainerMes1 {
    display: flex;
    justify-content: center;
    border-width: 0px;
    border-color: black;
    border-style: solid;
    background-color: #c0bfbd;
    font-weight: bold;
    height: 18px;
    width: 60px;
}

.numberContainerMes2 {
    display: flex;
    justify-content: center;
    border-width: 0px;
    border-color: black;
    border-style: solid;
    background-color: #c0bfbd;
    font-weight: bold;
    height: 18px;
    width: 60px;
}
    

.mesLinhasContainer {
    display: flex;
    margin-left: 50px;
}

.mesLinha {
    margin-top: 10px;
    margin-bottom: 10px;
}

.areaContainer {
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 100%;
    height: 112%;
    background-color: beige;
    padding-top: 10px;
    padding-bottom: 20px;
    border-left: 1px solid #00000032;          
}
.areaContainerMes {
    display: flex;
    align-items: center;
    flex-direction: column;    
    height: 100%;
    background-color: beige; 
    border-left: 1px solid #00000032;                
}

.areaContainerMes2 {
    display: flex;
    align-items: center;
    flex-direction: column;    
    height: 100%;
    background-color: beige;     
    border-left: 1px solid #00000032; 
    border-right: 1px solid #00000032;            
    
}
    

.osContainer {
    width: 95%;
    height: fit-content;
    display: flex;
    justify-content: center;
    background-color: #93d358;
    border-width: 1px;
    border-color: #73a744;
    border-style: solid;
    margin-top: 2px;
}

.btnAvancar,
.btnRetornar {
    font-size: 20px;
    padding: 10px;
    margin-bottom: 13px;
    border: none;
    border-radius: 5px;
}

.btnRetornar:hover,
.btnAvancar:hover {
    cursor: pointer;
    background-color: #d8d8d8;
}

.ContainerDepart,
.ContainerTipo {
    display: inline-block;
    background-color: white;

}

.ContainerDepart,
.ContainerTipoOs {
    width: 300px;
}

.departDiv {
    display: inline-flex;
}

.departName {
    font-size: 10px;
    background-color: white;
    height: 20px;
}

.departTittle {
    background-color: #08b0f1;
    border: black;
}

.departDa {
    display: flex;
    background-color: #08b0f1;
    font-size: 15px;
    animation: none;
    height: 23px;
    font-weight: bold;
    width: 150px;
}

.dropbtn {
    display: flex;
    background-color: white;
    font-size: 15px;
    animation: none;
    height: 23px;
    width: 150px;
}

.departamentClass {
    margin-top: 10px;
    margin-left: 7px;
}

.departTittle,
.departName {
    height: 20px;
}

.dispDia {
    display: inline-flex;
    width: 25x;
}

.horasTotaisDiv {
    display: inline-block;
    width: 25x;
    margin-left: 50px;
}

.nomeFuncDiv {
    display: inline-block;
}

.horasFuncDiv {
    display: inline-block;
    margin-left: 140px;
}

.funcHorasDiv {
    margin-bottom: 20px;
}


.tipoOsDiv {
    font-size: 15px;
    width: 150px;
    height: 20px;
}

.tipoOsTittle {
    display: inline-flex;
    background-color: #08b0f1;
    font-size: 15px;
    animation: none;
    height: 23px;
    font-weight: bold;
    width: 150px;
}

.tipoOSdivBtn {
    display: inline-block;
    align-items: left;
}

.tipoOSbtn {
    display: inline-flex;
    background-color: white;
    font-size: 15px;
    animation: none;
    width: 150px;
}

.dispDia {
    font-weight: bold;
}

.ContainerTipoOs {
    display: flex;
    height: auto;
}


.mainContainer {
    display: inline-flex;
    margin-top: 50px;
    margin-left: 50px;
}

.Container {
    display: inline-flex;
    flex-direction: column;
}

.dropdown,
.dropdown-os {
    position: relative;
    display: inline-block;
}

.dropdown-content,
.dropdown-content-tipo {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 150px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    padding: 12px 16px;
    z-index: 1;
    max-height: 240px;
    overflow-y: auto;
}

.dropdown-content a,
.dropdown-content-tipo a {
    display: block;
    text-decoration: none;
    color: black;
    margin-bottom: 10px;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown-os:hover .dropdown-content-tipo {
    display: block;
}

.descOsMainTittle {
  display: inline-flex;    
}

.descOsMainDefault{
  display: inline-flex;
  flex-direction: row;
}

.descDiv {
    display: flex;
    font-size: 15px;
    height: 22px;
    align-items: center;
    justify-content: center;
}

.descTittle {
    display: flex;
    font-size: 15px;
    height: 25px;
    align-items: center;
    justify-content: center;
    font-weight: bold;
    font-size: 17px;
}

.ordemOs{
    width: 90px;
    text-align: center;
}

.atividadeOs{
    width: 100px;
    text-align: center;
}

.descOs {
    width: 110px;
    text-align: center;
    border-left: 1px solid #00000032;
    border-bottom: 1px solid #00000032;
}

.descOsDefault {
    width: 110px;
    text-align: center;   
}

.descOsMp {
    width: 172px;
    text-align: center;
    border-left: 1px solid #00000032;
    border-bottom: 1px solid #00000032;
}

.descOsName {
    width: 190px;
    text-align: center;
    border-right: 1px solid #00000032;
    border-left: 1px solid #00000032;
    border-bottom: 1px solid #00000032;
}

.tipoOs {
    width: 90px;
    text-align: center;
}

.mpOs {
    width: 130px;
    text-align: center;
}

.hhmpOs {
    width: 120px;
    text-align: center;
}

.tMinOs {
    width: 90px;
    text-align: center;
}

.progOs {
    width: 90px;
    text-align: center;
}

.tMaxOs {
    width: 90px;
    text-align: center;
}

.maoObraOs {
    width: 130px;
    text-align: center;
}

.nomeFuncOs {
    width: 90px;
    text-align: center;
}

.containerDescOsMain {
    display: inline-flex;
    margin-top: 30px;
    margin-left: 50px;
}

.containerDescOs {
    display: inline-flex;
    flex-direction: column;    
}


.spanHoras {
    margin-left: 10px;
}

.mesTopClass {
    display: flex;
    align-items: center;    
    justify-content: center;
    margin-left: 50px;
}

.containerContainer {
    display: flex;
    flex-direction: column;
    margin-right: 280px;
}

.mesTopTotal {
    display: inline-flex;
}

.totalDiv {
    display: flex;
    align-items: top;
    justify-content: Left;
    margin-right: 3px;
    margin-top: 10px;

}

.horasTotal {
    margin-top: 5px;
    margin-left: 3px;
}

.horasTotalClass {
    padding-right: 40px;
    margin-top: 10px;
}

.undoBtnStyle {
    font-size: 20px;
    padding: 10px;
    margin-bottom: 13px;
    border: none;
    border-radius: 5px;
}

.undoContainer {
    display: flex;
    margin-left: 20px;
}

.undoBtnStyle:hover {
    cursor: pointer;
    background-color: #d8d8d8;
}

.naoSelecionar {
    user-select: none; /* Impede a seleção de texto */
    -moz-user-select: none; /* Firefox */
    -ms-user-select: none; /* Internet Explorer/Edge */
    -webkit-user-select: none; /* Safari */
}

.spanInfo {
  font-size: 14px
}

.mesLegendaContainer {
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  margin-left: 50px;
}

.legAzulDiv {
  background-color: #5e87fa44;
  height: 15px;
  width: 40px;
  margin-right: 5px;
  margin-top: 1px;
}

.legAmareloDiv {
  background-color: #ffca1a42;
  height: 15px;
  width: 40px;
  margin-right: 5px;
  margin-top: 1px;
}

.legVerdeDiv {
  background-color: #3df54d2d;
  height: 15px;
  width: 40px;
  margin-right: 5px;
  margin-top: 1px;
}

.legendasDiv {
  display: flex;
  flex-direction: column;
  justify-content: left;
  align-content: end;
}

</style>
<body>
    <section class="mesContainer">
        <div id="tituloContainer">
            <button id="mesAnterior" class="btnRetornar"><svg xmlns="http://www.w3.org/2000/svg" class="botaoSVG" height="24px" viewBox="0 -960 960 960" width="24px" fill="#383838"><path class="botaoSVG" d="M400-80 0-480l400-400 71 71-329 329 329 329-71 71Z"/></svg></button>
            <div id="mesNomeContainer" style= "display: flex; width: 170px; justify-content: center"></div>
            <button id="mesPosterior" class="btnAvancar"><svg xmlns="http://www.w3.org/2000/svg" class="botaoSVG" height="24px" viewBox="0 -960 960 960" width="24px" fill="#383838"><path class="botaoSVG" d="m321-80-71-71 329-329-329-329 71-71 400 400L321-80Z"/></svg></button>            
            <!--
            div class="undoContainer">
                <button class="undoBtnStyle" id="undoBtn"><svg xmlns="http://www.w3.org/2000/svg" height="27px" viewBox="0 -960 960 960" width="27px" fill="#383838"><path d="M280-200v-80h284q63 0 109.5-40T720-420q0-60-46.5-100T564-560H312l104 104-56 56-200-200 200-200 56 56-104 104h252q97 0 166.5 63T800-420q0 94-69.5 157T564-200H280Z"/></svg></button>
            </div>
            -->
        </div>        
        <div style="display: inline-flex"></div>
        <div class="contDetailsOs">
            <div></div>
        </div>
    </section> 
	<section class="mesLegendaContainer">
      <div class="legendasDiv">
        <div style="display: flex; flex-direction: row">
          <div class="legAzulDiv">&nbsp;&nbsp;</div>
          <div>Data Hoje</div>
        </div>
        <div style="display: flex; flex-direction: row">
          <div class="legAmareloDiv"></div>
          <div>Data Programação</div>
        </div>
		<div style="display: flex; flex-direction: row">
          <div class="legVerdeDiv"></div>
          <div>Data Disponível</div>
        </div>
      </div>
    </section>
    <div class="containerContainer">
        <div class="mesTopClass">
			<span id="mesTop">
		    </span>
		</div>        
        <section class="mesLinhasContainer">                
            <div class="monthContainer1" style="margin-top: 10px">
                <div id="divHorasTotaisOsDia-01" style="display: flex; justify-content: center; height: 17px;">
                    <span></span> 
                </div>
                <div class="numberContainer">
                    <p><svg xmlns="http://www.w3.org/2000/svg"height="18px" viewBox="0 -960 960 960" width="18px" fill="#383838"><path d="M400-80 0-480l400-400 71 71-329 329 329 329-71 71Z"/></svg></p>
                </div>
                <div class="areaContainerMes" id="mesAnterior" style="font-weight: bold;">
                    <p class="botaoSVG" style="font-weight: bold; margin-top: 20px">Mês</p>
                    <p class="botaoSVG" style="font-weight: bold">Anterior</p>
                </div>
            </div>       
            <section class="mesLinha mesLinha1" style="display: flex; flex-direction: row"></section> 
            <div class="monthContainer2" style="margin-top: 10px">
                <div id="divHorasTotaisOsDia-01" style="display: flex; justify-content: center; height: 17px;">
                    <span></span> 
                </div>
                <div class="numberContainer">
                    <p><svg xmlns="http://www.w3.org/2000/svg"height="18px" viewBox="0 -960 960 960" width="18px" fill="#383838"><path d="m321-80-71-71 329-329-329-329 71-71 400 400L321-80Z"/></svg></p>
                </div>
                <div class="areaContainerMes2" id="mesPosterior" style="font-weight: bold;">
                    <p class="botaoSVG" style="font-weight: bold; margin-top: 20px">Mês</p>
                    <p class="botaoSVG" style="font-weight: bold">Posterior</p>
                </div>
            </div>            
            <div class="totalDiv">Total: </div> <span id="horasTotal" class="horasTotalClass">0</span>
        </section>
    </div>
    <div class="mainContainer">
        <div class="Container">
            <div class="ContainerDepart">
                <div class="departDiv">
                    <div class="departTittle">
                        <button class="departDa">Departamento</button>
                    </div>
                    <div class="dropdown">
                        <button id="dropdown-btn"
                            class="dropbtn" style="text-align:left">Selecionar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<svg
                                xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 -960 960 960"
                                width="20px" fill="#000000">
                                <path d="M480-344 240-584l43-43 197 197 197-197 43 43-240 240Z" />
                            </svg></button>
                        <div class="dropdown-content" id="dropdown-content-id">
                            <a href="#" class="menu">DepartamentoTEST</a>
                        </div>
                    </div>
                </div>
                <div class="departamentClass" id="departamentId">
                    <div class="dispDia">
                        <h2 style="font-size: 15px" style="font-weight: bold" style="display: inline-block">
                            Disponibilidade DIA
                        </h2>
                    </div>
                    <div class="horasTotaisDiv">
                        <span class="horasTotais" id="horasTotais">36</span><br />
                    </div>
                    <div class="funcHorasDiv">
                        <div class="nomeFuncDiv">
                            <span class="nomeFunc" id="nomeFunc">Moraes</span><br />
                            <span>Bruno</span><br />
                            <span>Francisco</span><br />
                            <span>Henrique</span><br />
                            <span>Bruno</span><br />
                            <span>Francisco</span>
                        </div>
                        <div class="horasFuncDiv">
                            <span id="horasFunc">6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="ContainerTipoOs">
                <div class="tipoOsDiv">
                    <button class="tipoOsTittle">Tipo da OS</button>
                </div>
                <div class="dropdown-os">
                    <button id="dropdown-btn-os" class="dropbtn">Selecionar OS&nbsp;&nbsp;&nbsp;&nbsp;<svg
                            xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 -960 960 960"
                            width="20px" fill="#000000">
                            <path d="M480-344 240-584l43-43 197 197 197-197 43 43-240 240Z" />
                        </svg>
                    </button>
                    <div id="dropdown-content-tipo" class="dropdown-content-tipo">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="containerDescOsMain">
        <div class="containerDescOs">
            <div class="descOsMainTittle">
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Ordem</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Atividade</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Descrição</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Tipo</p>
                    </div>                
                </div>
                <div class="descOsMp">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">MP</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">HH MP</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">T.Min</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Prog</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">T.Max</p>
                    </div>                
                </div>
                <div class="descOs">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Mão de Obra</p>
                    </div>                
                </div>
                <div class="descOsName">
                    <div class="descDiv" style="background-color: #bfbfbf;">
                        <p class="descTittle">Nome</p>
                    </div>                
                </div>
            </div>
            <div id='htmlPageDescDiv' style="display: flex; flex-direction: column">
                <div class="descOsMainDefault">
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOsMp">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOs">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                  <div class="descOsName">        
                      <div class="descDiv">
                          <span class="spanInfo">-</span>
                      </div>
                  </div>
                </div>                                      
            </div>
        </div> 
    </div>
    </section>
</body>
</html>`;

let botaoAtivo = false;
let componenteArrastado;
let classeSelecionada;
let listaOsDesc;
let atividadeSelecionada;

getRecordId()
updateScheduleLabor()

function buscaInfoOs(id) {
	const dados = id.split('-');
	const osNumber = dados[0];
	const atividadeNumber = dados[1];
	const idProg = dados[2];
	const osSelecionada = listaOsDesc.filter((os) => os.os == osNumber);
	const listaAtividades = osSelecionada[0].atividades;
	const prog = listaAtividades.filter(
		(ativ) => ativ.atividade == atividadeNumber && ativ.id == idProg
	);
	prog.os = osNumber;
	return prog;
}

function getRecordId() {
	const myHeaders = new Headers();
	myHeaders.append('tenant', 'WDKRE1694712324_DEM');
	myHeaders.append('role', '*');
	myHeaders.append('organization', 'NRB');
	myHeaders.append('Accept', 'application/json');
	myHeaders.append(
		'Authorization',
		'Basic YnJ1bm8ucml2b2x0YUBucmJjb25zdWx0aW5nLmNvbTpOUkJjQDIwMjQ='
	);

	const requestOptions = {
		method: 'GET',
		headers: myHeaders,
		redirect: 'follow',
	};

	fetch(
		`https://us1.eam.hxgnsmartcloud.com/axis/restservices//workorders/12332%23NRB/schedulelabor/10%2312332%2310037`,
		requestOptions
	)
		.then((response) => response.text())
		.then((responseJson) => JSON.parse(responseJson))
		.then((result) => {
			console.log("RECORDID", result);
		})
		.catch((error) => console.error(error));
}

function updateScheduleLabor(data) {
	const [day, month, year] = data.split('-').map(Number);	
	const myHeaders = new Headers();

	myHeaders.append('tenant', 'WDKRE1694712324_DEM');
	myHeaders.append('role', '*');
	myHeaders.append('organization', 'NRB');
	myHeaders.append(
		'Authorization',
		'Basic YnJ1bm8ucml2b2x0YUBucmJjb25zdWx0aW5nLmNvbTpOUkJjQDIwMjQ='
	);
	myHeaders.append('Content-Type', 'application/json');
	myHeaders.append('Accept', 'application/json');

	const raw = JSON.stringify({
		SCHEDULEDDATE: {
			YEAR: year,
			MONTH: month,
			DAY: day,
		},
		recordid: 0,
		ignore_conflict: true,
	});

	const requestOptions = {
		method: 'PATCH',
		headers: myHeaders,
		body: raw,
		redirect: 'follow',
	};
	fetch(`https://us1.eam.hxgnsmartcloud.com/axis/restservices/workorders/12332%23NRB/schedulelabor/10%2312332%2310037`,requestOptions)
		.then((response) => response.text())
		.then((responseJson) => JSON.parse(responseJson))
		.then((result) => {
			console.log("UPDATE", result);
		})
		.catch((error) => console.error(error));
}

function gridDepart() {
	response = EAM.Ajax.request({
		url: 'GRIDDATA',
		params: {
			GRID_NAME: '1UDPRT',
			REQUEST_TYPE: 'LIST.HEAD_DATA.STORED',
			LOV_ALIAS_NAME_1: 'code',
			LOV_ALIAS_VALUE_1: 1,
			LOV_ALIAS_TYPE_1: '',
		},
		async: false,
		method: 'POST',
	});
	var gridresult = response.responseData.pageData.grid.GRIDRESULT.GRID;
	return gridresult;
}

function gridTipoOS() {
	responseTPOS = EAM.Ajax.request({
		url: 'GRIDDATA',
		params: {
			GRID_NAME: '1UTPOS',
			REQUEST_TYPE: 'LIST.HEAD_DATA.STORED',
			LOV_ALIAS_NAME_1: '',
			LOV_ALIAS_VALUE_1: '',
			LOV_ALIAS_TYPE_1: '',
		},
		async: false,
		method: 'POST',
	});
	var gridresultTipo =
		responseTPOS.responseData.pageData.grid.GRIDRESULT.GRID;
	return gridresultTipo;
}

function gridOS(dataInicio, dataFim) {
	let listaOS = [];
	let numeroLinhas = 100;
	let cursor = 1;

	for (let index = 0; index < 2000; index++) {
		responseOS = EAM.Ajax.request({
			url: 'GRIDDATA',
			params: {
				GRID_NAME: '1UORDE',
				REQUEST_TYPE: 'LIST.HEAD_DATA.STORED',
				NUMBER_OF_ROWS_FIRST_RETURNED: numeroLinhas,
				CURSOR_POSITION: cursor,
				LOV_ALIAS_NAME_1: 'dataInicio',
				LOV_ALIAS_VALUE_1: dataInicio,
				LOV_ALIAS_TYPE_1: '',
				LOV_ALIAS_NAME_2: 'dataFim',
				LOV_ALIAS_VALUE_2: dataFim,
				LOV_ALIAS_TYPE_2: '',
			},
			async: false,
			method: 'POST',
		});
		var gridresultOS =
			responseOS.responseData.pageData.grid.GRIDRESULT.GRID.DATA;

		if (gridresultOS.length == 0) {
			break;
		}

		cursor = cursor + numeroLinhas;
		let listaNova = listaOS.concat(gridresultOS);
		listaOS = listaNova;
	}
	let listaOsFormatada = formatandoGridOs(listaOS);

	return listaOsFormatada;
}

function formatandoGridOs(requisicao) {
	let json = [];
	let id = 1;

	for (let index = 0; index < requisicao.length; index++) {
		if (
			requisicao[index].acs_sched != '' &&
			requisicao[index].acs_sched != null &&
			requisicao[index].acs_sched != undefined &&
			requisicao[index].acs_hours != '' &&
			requisicao[index].acs_hours != null &&
			requisicao[index].acs_hours != undefined &&
			requisicao[index].evt_type != '' &&
			requisicao[index].evt_type != null &&
			requisicao[index].evt_type != undefined &&
			requisicao[index].evt_target != '' &&
			requisicao[index].evt_target != null &&
			requisicao[index].evt_target != undefined &&
			requisicao[index].evt_requeststart != '' &&
			requisicao[index].evt_requeststart != null &&
			requisicao[index].evt_requeststart != undefined &&
			requisicao[index].evt_requestend != '' &&
			requisicao[index].evt_requestend != null &&
			requisicao[index].evt_requestend != undefined &&
			requisicao[index].act_act != '' &&
			requisicao[index].act_act != null &&
			requisicao[index].act_act != undefined
		) {
			let detalhes = {
				id: id,
				atividade: requisicao[index].act_act,
				dataInicioProgr: new Date(
					Number(requisicao[index].evt_target.substring(6, 10)),
					Number(requisicao[index].evt_target.substring(3, 5) - 1),
					Number(requisicao[index].evt_target.substring(0, 2))
				),
				codigoProg: requisicao[index].acs_code,
				tipoOS: requisicao[index].evt_jobtype,
				descricaoTipoOs: requisicao[index].evt_type,
				maoDeObra: requisicao[index].acs_responsible,
				departamento: requisicao[index].evt_mrc,
				mp: requisicao[index].evt_ppm,
				descricaoOS: requisicao[index].evt_note,
				toleranciaMin: new Date(
					Number(requisicao[index].evt_requeststart.substring(6, 10)),
					Number(
						requisicao[index].evt_requeststart.substring(3, 5) - 1
					),
					Number(requisicao[index].evt_requeststart.substring(0, 2))
				),
				toleranciaMax: new Date(
					Number(requisicao[index].evt_requestend.substring(6, 10)),
					Number(
						requisicao[index].evt_requestend.substring(3, 5) - 1
					),
					Number(requisicao[index].evt_requestend.substring(0, 2))
				),
				mmhp: requisicao[index].acs_hours,
				funcionario: requisicao[index].acsresponsibledesc,
				dataProgramacao: new Date(
					Number(requisicao[index].acs_sched.substring(6, 10)),
					Number(requisicao[index].acs_sched.substring(3, 5) - 1),
					Number(requisicao[index].acs_sched.substring(0, 2))
				),
			};
			let osExistente;
			for (let i = 0; i < json.length; i++) {
				if (json[i].os == requisicao[index].evt_code) {
					osExistente = i;
				}
			}
			if (osExistente != undefined) {
				id++;
				json[osExistente].atividades.push(detalhes);
			} else {
				id++;
				let objeto = {
					os: requisicao[index].evt_code,
					atividades: [],
				};
				objeto.atividades.push(detalhes);
				json.push(objeto);
			}
		}
	}
	return json;
}

function eventoClickMenu() {
	let listaOpcoesMenu = document.getElementsByClassName('menu');

	for (let index = 0; index < listaOpcoesMenu.length; index++) {
		listaOpcoesMenu[index].addEventListener(
			'click',
			function () {
				let depart = listaOpcoesMenu[index].innerHTML;
				selectDepartment(depart);
			},
			false
		);
	}
}

function selectDepartment(department) {
	let menuBotao = document.getElementById('dropdown-btn');
	menuBotao.textContent = department;
}

function eventoClickMenuOs() {
	let listaOpcoesMenuOs = document.getElementsByClassName('menu-os');

	for (let index = 0; index < listaOpcoesMenuOs.length; index++) {
		listaOpcoesMenuOs[index].addEventListener(
			'click',
			function () {
				let os = listaOpcoesMenuOs[index].innerHTML;
				selectOs(os);
			},
			false
		);
	}
}

function selectOs(Os) {
	let menuBotaoOs = document.getElementById('dropdown-btn-os');
	menuBotaoOs.textContent = Os;
}

let dadosMes;
let mesFormatado;

function configuracoesDeTela() {
	try {
		Ext.ComponentQuery.query('uxtabpanel')[0].el.dom.style.height = '0px';
		var referencenode =
			Ext.ComponentQuery.query('uxtabpanel')[0].up().body.dom;
		vWidth = referencenode.clientWidth;
		vHeigh = referencenode.clientHeight;
		Ext.ComponentQuery.query('[uftId=resetrec]')[0].enable();
		Ext.ComponentQuery.query('[uftId=enterdesigner]')[0].disable();
		Ext.ComponentQuery.query('[uftId=epak]')[0].disable();
		Ext.ComponentQuery.query('[uftId=help]')[0].disable();
	} catch (err) {
		console.log(err);
	}

	var vCustomDiv = document.getElementById('custom-div');
	if (vCustomDiv) {
		vCustomDiv.parentElement.removeChild(vCustomDiv);
	}

	var node = document.createElement('div');
	node.id = 'custom-div';
	node.style.width = '100%';
	node.style.height = '100%';
	referencenode.appendChild(node);

	var divcustom = document.getElementById('custom-div');
	divcustom.innerHTML = htmlPage;
}

function gerarDadosMes(valor = null) {
	let dataAtual = new Date();
	let mes;
	let ano;

	if (valor == null) {
		mes = dataAtual.getMonth() + 1;
		ano = dataAtual.getFullYear();
	}

	if (valor == 'posterior') {
		if (dadosMes.mes == 12) {
			mes = 1;
			ano = dadosMes.ano + 1;
		} else {
			mes = dadosMes.mes + 1;
			ano = dadosMes.ano;
		}
	}

	if (valor == 'anterior') {
		if (dadosMes.mes == 1) {
			mes = 12;
			ano = dadosMes.ano - 1;
		} else {
			mes = dadosMes.mes - 1;
			ano = dadosMes.ano;
		}
	}

	let data = new Date(ano, mes, 0);
	let diasNoMes = data.getDate();

	let mesString =
		mes == 1
			? 'Janeiro'
			: mes == 2
			? 'Fevereiro'
			: mes == 3
			? 'Março'
			: mes == 4
			? 'Abril'
			: mes == 5
			? 'Maio'
			: mes == 6
			? 'Junho'
			: mes == 7
			? 'Julho'
			: mes == 8
			? 'Agosto'
			: mes == 9
			? 'Setembro'
			: mes == 10
			? 'Outubro'
			: mes == 11
			? 'Novembro'
			: mes == 12
			? 'Dezembro'
			: 'error';

	dadosMes = {
		dias: diasNoMes,
		mes: mes,
		ano: ano,
	};

	mesFormatado = formataMes(mes);

	let mesNomeContainer = document.getElementById('mesNomeContainer');
	mesNomeContainer.innerHTML = `<h1 class="tituloMes" style="font-size: 21px">${mesString} / ${ano}</h1>`;

	let mesNomeTop = document.getElementById('mesTop');
	mesNomeTop.innerHTML = `<p class= tituloMesTop>${mesString}/${ano}</p>`;

	let dataInicio = `${
		mes.toString().length == 1 ? '0' + mes.toString() : mes.toString()
	}-01-${ano}`;
	let dataFim = `${
		mes.toString().length == 1 ? '0' + mes.toString() : mes.toString()
	}-${diasNoMes}-${ano}`;
	listaOsDesc = gridOS(dataInicio, dataFim);
}

function verificaEventosBotaoTrocaMes() {
	document
		.getElementById('mesAnterior')
		.addEventListener('click', function () {
			gerarDadosMes('anterior');
			geraCalendarioMes();
			geraOS();
			atualizaHorasTotaisPorDia();
			totalHorasMes();
			eventoClickDescOs();
		});
	document
		.getElementById('mesPosterior')
		.addEventListener('click', function () {
			gerarDadosMes('posterior');
			geraCalendarioMes();
			geraOS();
			atualizaHorasTotaisPorDia();
			totalHorasMes();
			eventoClickDescOs();
		});
}

function verificaEventosBotaoTrocaMesGrid() {
	document
		.getElementById('areaContainerMes')
		.addEventListener('click', function () {
			gerarDadosMes('anterior');
			geraCalendarioMes();
			geraOS();
			atualizaHorasTotaisPorDia();
			totalHorasMes();
			eventoClickDescOs();
		});
	document
		.getElementById('areaContainerMes2')
		.addEventListener('click', function () {
			gerarDadosMes('posterior');
			geraCalendarioMes();
			geraOS();
			atualizaHorasTotaisPorDia();
			totalHorasMes();
			eventoClickDescOs();
		});
}

function geraCalendarioMes() {
	var sectionMes1 = document.getElementsByClassName('mesLinha1')[0];
	sectionMes1.innerHTML = '<div></div>';

	let mesLinha1 = [];

	for (let dia = 1; dia < dadosMes.dias + 1; dia++) {
		let diaFormatado = formataDia(dia);
		var diaComponent = [
			`<div class="dayContainer">`,
			`    <div style="display: flex; justify-content: center; align-text: center" id="divHorasTotaisOsDia-${diaFormatado}">`,
			`    <span class="" id="horasTotaisOsDia-${diaFormatado}">0</span><span>h</span>`,
			`    </div>`,
			`    <div class="numberContainer" id="${diaFormatado}-${mesFormatado}-${dadosMes.ano}-diaCor">`,
			`        <p>${dia}</p>`,
			`    </div>`,
			`    <div `,
			`        class="areaContainer" `,
			`        id="${diaFormatado}-${mesFormatado}-${dadosMes.ano}"`,
			`    >`,
			`    </div>`,
			`</div>`,
		].join('');

		if (dia <= 31) {
			mesLinha1.push(diaComponent);
		}
	}
	let mesHtml1 = mesLinha1.join('');
	sectionMes1.innerHTML = mesHtml1;
	adicionaDropDragOver(dadosMes.dias);
}

function adicionaDropDragOver(dias) {
	for (let dia = 1; dia < dias + 1; dia++) {
		let diaFormatado = formataDia(dia);
		document
			.getElementById(`${diaFormatado}-${mesFormatado}-${dadosMes.ano}`)
			.addEventListener('drop', async function (event) {
				event.preventDefault();

				const osInfo = buscaInfoOs(componenteArrastado.id);

				const data = event.dataTransfer.getData('Text');
				if (event.target.className == 'areaContainer') {
					event.target.style.backgroundColor = 'beige';
					event.target.style.border = '';
					try {
						event.target.appendChild(componenteArrastado);
					} catch (error) {
						console.log(error);
					}
				}
			});
		document
			.getElementById(`${diaFormatado}-${mesFormatado}-${dadosMes.ano}`)
			.addEventListener('dragover', function (event) {
				event.preventDefault();
			});
	}
	document.addEventListener('dragenter', function (event) {
		classeSelecionada = event.target.getAttribute('class');
		if (event.target.className == 'areaContainer') {
			event.target.style.backgroundColor = '#d6d6b8';
			event.target.style.border = '1px dashed black';
		}
		if (
			event.target.className == 'btnRetornar' ||
			event.target.className == 'areaContainerMes' ||
			event.target.className == 'botaoSVG'
		) {
			if (botaoAtivo == false) {
				document.getElementById('mesAnterior').style.backgroundColor =
					'#d8d8d8;';
				document.getElementById('custom-div').style.cursor = 'progress';
				botaoAtivo = true;
				setTimeout(() => {
					if (
						classeSelecionada == 'btnRetornar' ||
						classeSelecionada == 'areaContainerMes' ||
						classeSelecionada == 'botaoSVG'
					) {
						gerarDadosMes('anterior');
						geraCalendarioMes();
						geraOS();
						atualizaHorasTotaisPorDia();
						totalHorasMes();
						document.getElementById('custom-div').style.cursor =
							'default';
					} else {
						document.getElementById('custom-div').style.cursor =
							'default';
					}
				}, 2000);
				setTimeout(() => {
					botaoAtivo = false;
				}, 3000);
			}
		}
		if (
			event.target.className == 'btnAvancar' ||
			event.target.className == 'areaContainerMes2' ||
			event.target.className == 'botaoSVG'
		) {
			if (botaoAtivo == false) {
				document.getElementById('mesPosterior').style.backgroundColor =
					'#d8d8d8;';
				document.getElementById('custom-div').style.cursor = 'progress';
				botaoAtivo = true;
				setTimeout(() => {
					if (
						classeSelecionada == 'btnAvancar' ||
						classeSelecionada == 'areaContainerMes2' ||
						classeSelecionada == 'botaoSVG'
					) {
						gerarDadosMes('posterior');
						geraCalendarioMes();
						geraOS();
						atualizaHorasTotaisPorDia();
						totalHorasMes();
						document.getElementById('custom-div').style.cursor =
							'default';
					} else {
						document.getElementById('custom-div').style.cursor =
							'default';
					}
				}, 2000);
				setTimeout(() => {
					botaoAtivo = false;
				}, 3000);
			}
		}
	});
	document.addEventListener('dragleave', function (event) {
		if (event.target.className == 'areaContainer') {
			event.target.style.backgroundColor = 'beige';
			event.target.style.border = '';
		}
	});
}

function geraOS() {
	let areaDia;
	for (let iOs = 0; iOs < listaOsDesc.length; iOs++) {
		for (
			let index = 0;
			index < listaOsDesc[iOs].atividades.length;
			index++
		) {
			let dataFormatada = `${formataDia(
				listaOsDesc[iOs].atividades[index].dataProgramacao.getDate()
			)}-${formataMes(
				listaOsDesc[iOs].atividades[index].dataProgramacao.getMonth() +
					1
			)}-${listaOsDesc[iOs].atividades[
				index
			].dataProgramacao.getFullYear()}`;
			let totalDataHora = 0;
			for (
				let num = 0;
				num < listaOsDesc[iOs].atividades[index].mmhp.length;
				num++
			) {
				totalDataHora += Number(
					listaOsDesc[iOs].atividades[index].mmhp[num]
				);
			}

			areaDia = document.getElementById(dataFormatada);

			let osExistente = areaDia.innerHTML;
			var os = [
				`<div `,
				`    id="${listaOsDesc[iOs].os}-${listaOsDesc[iOs].atividades[index].atividade}-${listaOsDesc[iOs].atividades[index].id}"`, //
				`    class="osContainer"`,
				`    draggable="true"`,
				`    data-horas="${totalDataHora}"`,
				`    data-dia="${dataFormatada}}"`,
				`>`,
				`    ${listaOsDesc[iOs].os}`,
				`</div>`,
			].join('');
			areaDia.innerHTML = osExistente + os;
		}
	}
	adicionaDragStartDragEnd(listaOsDesc);
}

function atualizaHorasTotaisPorDia() {
	for (let dia = 1; dia < dadosMes.dias + 1; dia++) {
		let diaFormatado = formataDia(dia);
		let areaDia = document.getElementById(
			`${diaFormatado}-${mesFormatado}-${dadosMes.ano}`
		);
		let osContainers = areaDia.getElementsByClassName('osContainer');
		let osDia = areaDia.children;
		let totalHoras = 0;

		for (let os of osContainers) {
			totalHoras += parseInt(os.getAttribute('data-horas'));
		}

		let horasTotaisElement = document.getElementById(
			`horasTotaisOsDia-${diaFormatado}`
		);
		horasTotaisElement.textContent = `${totalHoras}`;

		if (totalHoras >= 37) {
			for (let index = 0; index < osDia.length; index++) {
				osDia[index].style.backgroundColor = '#e9615c'; //RED
				osDia[index].style.borderColor = '#a2413e';
			}
		} else if (totalHoras >= 32 && totalHoras <= 36) {
			for (let index = 0; index < osDia.length; index++) {
				osDia[index].style.backgroundColor = '#FFBF2E'; //YELL
				osDia[index].style.borderColor = '#b1841a';
			}
		} else {
			for (let index = 0; index < osDia.length; index++) {
				osDia[index].style.backgroundColor = '#93d358'; //GREEN
				osDia[index].style.borderColor = '#73a744';
			}
		}
	}
}

function totalHorasMes() {
	let totalHoras = 0;
	for (let dia = 1; dia <= 31; dia++) {
		let diaFormatado = dia.toString().padStart(2, '0');
		let elementoId = 'horasTotaisOsDia-' + diaFormatado;
		let elemento = document.getElementById(elementoId);

		if (elemento) {
			let horasInt = Number(elemento.textContent.trim());
			if (!isNaN(horasInt)) {
				totalHoras += horasInt;
			}
		}
	}
	document.getElementById('horasTotal').innerHTML = totalHoras + 'h';
}

function adicionaDragStartDragEnd(idList) {
	for (let id = 0; id < idList.length; id++) {
		for (let index = 0; index < idList[id].atividades.length; index++) {
			let mes = formataMes(
				idList[id].atividades[index].dataProgramacao.getMonth() + 1
			);
			if (mesFormatado == mes) {
				document
					.getElementById(
						`${idList[id].os}-${idList[id].atividades[index].atividade}-${idList[id].atividades[index].id}`
					)
					.addEventListener('dragstart', function (event) {
						event.dataTransfer.setData('Text', event.target.id);
						//salvarEstado();.
						componenteArrastado = document.getElementById(
							event.target.id
						);
						event.target.style.opacity = '0.4';
						corTolerancia();
					});
				document
					.getElementById(
						`${idList[id].os}-${idList[id].atividades[index].atividade}-${idList[id].atividades[index].id}`
					)
					.addEventListener('dragend', function (event) {
						event.target.style.opacity = '1';
						atualizaHorasTotaisPorDia();
						totalHorasMes();
						corTolerancia();
					});
			}
		}
	}
}

function formataMes(mes) {
	let mesString = mes.toString();
	let mesFormatado = mesString.length == 1 ? `0${mesString}` : mesString;
	return mesFormatado;
}

function formataDia(dia) {
	let diaString = dia.toString();
	let diaFormatado = diaString.length == 1 ? `0${diaString}` : diaString;
	return diaFormatado;
}

function pegarDepartamentos() {
	const depart = gridDepart();
	const divDepartamentos = document.getElementById('dropdown-content-id');
	divDepartamentos.innerHTML = '';
	depart.DATA.forEach((item) => {
		const addTag = document.createElement('a');
		addTag.setAttribute('href', '#');
		addTag.setAttribute('class', 'menu');
		addTag.textContent = item.mrc_code;
		divDepartamentos.appendChild(addTag);
	});
	eventoClickMenu();
}

function pegarTipo() {
	const Tipo = gridTipoOS();
	const divTipoOS = document.getElementById('dropdown-content-tipo');
	divTipoOS.innerHTML = '';
	Tipo.DATA.forEach((item) => {
		const addTag = document.createElement('a');
		addTag.setAttribute('href', '#');
		addTag.setAttribute('class', 'menu-os');
		let osNomeBruto = item.uco_desc;
		let osNomeCorreto = osNomeBruto.slice(11);
		addTag.textContent =
			osNomeCorreto[0].toUpperCase() + osNomeCorreto.substring(1);
		divTipoOS.appendChild(addTag);
	});
	eventoClickMenuOs();
}

function eventoClickDescOs() {
	const divs = document.querySelectorAll('.osContainer');
	divs.forEach((div) => {
		div.addEventListener('click', function (event) {            
			atividadeSelecionada = buscaInfoOs(event.target.id); 
            corTolerancia()          
			const divHtml = document.getElementById('htmlPageDescDiv');

			linhaHtml = `
				<div class="hyperDescOs" style="display: flex">
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada.os}</span>
					</div>
				</div>
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada[0].atividade}</span>
					</div>
				</div>
				<div class="descOs">
					<div class="descDiv">
						<span class="spanInfo">${
							atividadeSelecionada[0].descricaoOS == undefined
								? '-'
								: atividadeSelecionada[0].descricaoOS
						}</span>
					</div>
				</div>
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada[0].descricaoTipoOs}</span>
					</div>
				</div>
				<div class="descOsMp">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada[0].mp}</span>
					</div>
				</div>
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada[0].mmhp}</span>
					</div>
				</div>
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${formataDia(
							atividadeSelecionada[0].toleranciaMin.getDate()
						)}/${formataMes(
                            atividadeSelecionada[0].toleranciaMin.getMonth() + 1
                        )}/${atividadeSelecionada[0].toleranciaMin.getFullYear()}</span>
					</div>
				</div>
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${formataDia(
							atividadeSelecionada[0].dataProgramacao.getDate()
						)}/${formataMes(
                            atividadeSelecionada[0].dataProgramacao.getMonth() + 1
                        )}/${atividadeSelecionada[0].dataProgramacao.getFullYear()}</span>
					</div>
				</div>
				<div class="descOs">
					<div class="descDiv">
						<span class="spanInfo">${formataDia(
							atividadeSelecionada[0].toleranciaMax.getDate()
						)}/${formataMes(
                            atividadeSelecionada[0].toleranciaMax.getMonth() + 1
                        )}/${atividadeSelecionada[0].toleranciaMax.getFullYear()}</span>
					</div>
				</div>
				<div class="descOs">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada[0].maoDeObra}</span>
					</div>
				</div>
				<div class="descOsName">        
					<div class="descDiv">
						<span class="spanInfo">${atividadeSelecionada[0].funcionario}</span>
					</div>
				</div>
				</div>`;
			divHtml.innerHTML = linhaHtml;
		});
	});
}

function barraRolagem() {
	const customDiv = document.getElementById('custom-div');
	if (customDiv) {
		customDiv.style.overflow = 'auto';
		customDiv.style.height = '98%';
	}
}

function corTolerancia(){
    let tMin = atividadeSelecionada[0].toleranciaMin
    let tMax = atividadeSelecionada[0].toleranciaMax
	let dataProg = atividadeSelecionada[0].dataProgramacao	
	let hoje = new Date();
	
	const formatDate = (date) => {
        return `${formataDia(date.getDate())}-${formataMes(date.getMonth() + 1)}-${date.getFullYear()}-diaCor`;
    };
	
    let currentDate = new Date(dataProg);
    currentDate.setHours(0, 0, 0, 0);

	formatDataProg = formatDate(dataProg)
	formatDatatMin = formatDate(tMin)

    while (currentDate <= tMax) {
        let formatCurrentDate = formatDate(currentDate);
        let gridElement = document.getElementById(formatCurrentDate);

		if (gridElement) {
            gridElement.style.backgroundColor = '#3df54d2d'; 
        } 
		const elementos = document.querySelectorAll('.containerContainer');
		elementos.forEach((element) => { 
			console.log(element)
			if(element.id >= formatDatatMin && element.id < formatDataProg){
				document.getElementById(element.id).style.backgroundColor = 'beige'
			}				
		});
        currentDate.setDate(currentDate.getDate() + 1);
    }    

	let hojeFormatado = formatDate(hoje)	
	document.getElementById(hojeFormatado).style.backgroundColor = '#5e87fa44'
	document.getElementById(formatDataProg).style.backgroundColor = '#ffca1a42' 
}	

Ext.define('EAM.custom.external_devcal', {
	extend: 'EAM.custom.AbstractExtensibleFramework',
	getSelectors: function () {
		return {
			'[extensibleFramework] [tabName=HDR][isTabView=true]': {
				afterRender: function () {
					console.log('Teste: afterrender');
				},
				afterloaddata: function () {
					console.log('Teste: afterload');
				},
				afterlayout: function () {
					try {
						if (
							EAM.Utils.getScreen().userFunction == 'DEVCAL' &&
							Ext.ComponentQuery.query('uxtabpanel')[0]
						) {
							configuracoesDeTela();
							verificaEventosBotaoTrocaMes();
							gerarDadosMes();
							eventoClickMenu();
							eventoClickMenuOs();
							pegarDepartamentos();
							geraCalendarioMes();
							geraOS();
							atualizaHorasTotaisPorDia();
							pegarTipo();
							totalHorasMes();
							eventoClickDescOs();
							barraRolagem();
						}
					} catch (err) {
						console.log(err);
					}
				},
			},
		};
	},
});
