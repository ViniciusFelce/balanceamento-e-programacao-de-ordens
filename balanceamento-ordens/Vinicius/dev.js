let htmlPage = `
<!DOCTYPE html> 
<html lang="pt-br"> 
<head> 
    <meta charset="UTF-8"> 
    <meta http-equiv="Pragma" content="no-cache" /> 
    <meta http-equiv="Expires" content="-1" /> 
    <meta http-equiv="Cache-Control" content="no-cache" /> 
    <title>Calendário</title> 
</head> 
<style>
p {
    margin: 0;
}

.tituloMes {
    font-size: 18px;
    padding: 5px 5px 15px 5px;
}

#tituloContainer {
    display: flex;
    flex-direction: row;
    align-items: center;
}

.dayContainer {
    width: 55px;
    height: auto;
}
    

.numberContainer {
    display: flex;
    justify-content: center;
    border-width: 0px;
    border-color: black;
    border-style: solid;
    background-color: #c0bfbd;
}

.mesLinhasContainer {  
    display: flex;
    margin-left: 100px;       
}

.mesLinha {
    margin-top: 10px;
    margin-bottom: 10px;
}

.areaContainer {
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 100%;
    height: 100%;
    background-color: beige;
    padding-top: 10px;
    padding-bottom: 20px;
}

.osContainer {
    width: 95%;
    height: fit-content;
    display: flex;
    justify-content: center;
    background-color: #93d358;
    border-width: 1px;
    border-color: #73a744;
    border-style: solid;
    margin-top: 2px;
}

.botaoAvancarRetornar {
    font-size: 20px;
    padding: 10px;
    margin-bottom: 13px;
}

.botaoAvancarRetornar:hover {
    cursor: pointer;
}

.ContainerDepart, .ContainerTipo {
    display: inline-block;
    background-color: white;
}

.ContainerDepart, .ContainerTipoOs {        
    width: 300px;
}

.departDiv {
    display: inline-flex;
}

.departName {
    font-size: 10px;
    background-color: white;
    height: 20px;
}

.departTittle {
    background-color: #08b0f1;
    border: black;
}

.departDa {
    display: flex;
    background-color: #08b0f1;
    font-size: 15px;
    animation: none;
    height: 23px;
    font-weight: bold;
    width: 150px;
}

.dropbtn {
    display: flex;
    background-color: white;
    font-size: 15px;
    animation: none;
    height: 23px;
    width: 150px;
}

.departamentClass {
    margin-top: 10px;
    margin-left: 7px;
}

.departTittle,.departName {
    height: 20px;
}

.dispDia {
    display: inline-flex;
    width: 25x;
}

.horasTotaisDiv {
    display: inline-block;
    width: 25x;
    margin-left: 50px;
}

.nomeFuncDiv {
    display: inline-block;
}

.horasFuncDiv {
    display: inline-block;
    margin-left: 140px;
}

.funcHorasDiv {
    margin-bottom: 20px;
}


.tipoOsDiv {
    font-size: 15px;
    width: 150px;
    height: 20px;
}

.tipoOsTittle {
    display: inline-flex;
    background-color: #08b0f1;
    font-size: 15px;
    animation: none;
    height: 23px;
    font-weight: bold;
    width: 150px;
}

.tipoOSdivBtn {
    display: inline-block;
    align-items: left;
}

.tipoOSbtn {
    display: inline-flex;
    background-color: white;
    font-size: 15px;
    animation: none;
    width: 150px;
}

.dispDia {
    font-weight: bold;
}

.ContainerTipoOs {
    display: flex;
    height: 225px;
}

.mainContainer {
    display: inline-flex;
    margin-top: 5px;
    margin-left: 100px;    
}

.Container {
    display: inline-flex;
    flex-direction: column;
}

.dropdown, .dropdown-os {
    position: relative;
    display: inline-block;
}

.dropdown-content, .dropdown-content-tipo {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 150px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    padding: 12px 16px;
    z-index: 1;
    max-height: 240px;
    overflow-y: auto;
}

.dropdown-content a, .dropdown-content-tipo a {
    display: block;
    text-decoration: none;
    color: black;
    margin-bottom: 10px;    
}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown-os:hover .dropdown-content-tipo {
    display: block;
}

.descOsMain {
    display: inline-flex;

}

.descDiv {
    display: flex;
    font-size: 15px;
    height: 25px;
    align-items: center;
    justify-content: center;
}

.descTittle {
    display: flex;
    font-size: 15px;
    height: 25px;
    align-items: center;
    justify-content: center;
    font-weight: bold;
    font-size: 17px;
}

.ordemOs {
    width: 85px;
    text-align: center;
}

.descOs {
    width: 90px;
    text-align: center;
}

.tipoOs {
    width: 80px;
    text-align: center;
}

.mpOs {
    width: 100px;
    text-align: center;
}

.hhmpOs {
    width: 90px;
    text-align: center;
}

.tMinOs {
    width: 80px;
    text-align: center;
}

.progOs {
    width: 90px;
    text-align: center;
}

.tMaxOs {
    width: 70px;
    text-align: center;
}

.maoObraOs {
    width: 130px;
    text-align: center;
}

.containerDescOs {
    display: inline-flex;
    margin-top: 5px;
    margin-left: 20px
    
}

.spanHoras{
    margin-left: 10px;
}

.mesTopClass {
    display: flex;
    align-items: center;
    justify-content: center; 
    margin-right: 200px;
}

.mesTopTotal {
    display: inline-flex;
    
    
}

.totalDiv {
    display: flex;
    align-items: top;
    justify-content: Left;  
    margin-right: 3px;

}

.horasTotal {
    margin-top: 5px;
    margin-left: 3px;
}

</style>

<body>
    <section class="mesContainer">
        <div id="tituloContainer">
            <p id="mesAnterior" class="botaoAvancarRetornar">&lt;</p>
            <div id="mesNomeContainer"></div>
            <p id="mesPosterior" class="botaoAvancarRetornar">&gt;</p>
        </div>
        <div style="display: inline-flex">
            
        </div>
        <div class="contDetailsOs">
            <div></div>
        </div>
    </section>      
    <div class="mesTopClass"><span id="mesTop"></span></div>        
    <section class="mesLinhasContainer"> 
                    
        <section class="mesLinha mesLinha1" style="display: flex; flex-direction: row"></section>
        <section class="mesLinha mesLinha2" style="display: flex; flex-direction: row"></section>
        <div class="totalDiv">Total: </div> <span id="horasTotal">720</span>
    </section>
    </div>
    <div class="mainContainer">
        <div class="Container">
            <div class="ContainerDepart">
                <div class="departDiv">
                    <div class="departTittle">
                        <button class="departDa">Departamento</button>
                    </div>
                    <div class="dropdown">
                        <button id="dropdown-btn"
                            class="dropbtn">Selecionar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<svg
                                xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 -960 960 960"
                                width="20px" fill="#000000">
                                <path d="M480-344 240-584l43-43 197 197 197-197 43 43-240 240Z" />
                            </svg></button>
                        <div class="dropdown-content" id="dropdown-content-id">
                            <a href="#" class="menu">DepartamentoTEST</a>

                        </div>
                    </div>
                </div>
                <div class="departamentClass" id="departamentId">
                    <div class="dispDia">
                        <h2 style="font-size: 15px" style="font-weight: bold" style="display: inline-block">
                            Disponibilidade DIA
                        </h2>
                    </div>
                    <div class="horasTotaisDiv">
                        <span class="horasTotais" id="horasTotais">36</span><br />
                    </div>
                    <div class="funcHorasDiv">
                        <div class="nomeFuncDiv">
                            <span class="nomeFunc" id="nomeFunc">Moraes</span><br />
                            <span>Bruno</span><br />
                            <span>Francisco</span><br />
                            <span>Henrique</span><br />
                            <span>Bruno</span><br />
                            <span>Francisco</span>
                        </div>
                        <div class="horasFuncDiv">
                            <span id="horasFunc">6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                            <span>6</span><br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="ContainerTipoOs">
                <div class="tipoOsDiv">
                    <button class="tipoOsTittle">Tipo da OS</button>
                </div>
                <div class="dropdown-os">
                    <button id="dropdown-btn-os" class="dropbtn">Selecionar OS&nbsp;&nbsp;&nbsp;&nbsp;<svg
                            xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 -960 960 960"
                            width="20px" fill="#000000">
                            <path d="M480-344 240-584l43-43 197 197 197-197 43 43-240 240Z" />
                        </svg>
                    </button>
                    <div id="dropdown-content-tipo" class="dropdown-content-tipo">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="containerDescOs">
        <div class="descOsMain">
            <div class="ordemOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">Ordem</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="ordemInfo">OS:203312</span>
                </div>
            </div>
            <div class="descOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">Descrição</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="descInfo">Calibração</span>
                </div>
            </div>
            <div class="tipoOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">Tipo</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="tipoInfo">Preventiva</span>
                </div>
            </div>
            <div class="mpOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">MP</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="mpInfo">CAL_PIT_48M</span>
                </div>
            </div>
            <div class="hhmpOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">HH MP</p>
                </div>
                <div class="descDiv" id="descInfo">
                    <span class="spanInfo" id="hhInfo">4h</span>
                </div>
            </div>
            <div class="tMinOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">T.Min</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="tMinInfo">10/12/2023</span>
                </div>
            </div>
            <div class="progOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">Prog</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="progInfo">02/01/2024</span>
                </div>
            </div>
            <div class="tMaxOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">T.Max</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="tMaxInfo">29/01/2024</span>
                </div>
            </div>
            <div class="maoObraOs">
                <div class="descDiv" style="background-color: #bfbfbf;">
                    <p class="descTittle">Mão de Obra</p>
                </div>
                <div class="descDiv">
                    <span class="spanInfo" id="maoObraInfo">150186</span>
                </div>
            </div>
        </div>
    </div>
    </section>
</body>
</html>`;

function gridDepart() {
    response = EAM.Ajax.request({
        url: 'GRIDDATA',
        params: {
            GRID_NAME: '1UDPRT',
            REQUEST_TYPE: 'LIST.HEAD_DATA.STORED',
            LOV_ALIAS_NAME_1: 'code',
            LOV_ALIAS_VALUE_1: 1,
            LOV_ALIAS_TYPE_1: '',
        },
        async: false,
        method: 'POST',
    });
    var gridresult = response.responseData.pageData.grid.GRIDRESULT.GRID;
    return gridresult;
}

function gridTipoOS() {
    responseTPOS = EAM.Ajax.request({
        url: 'GRIDDATA',
        params: {
            GRID_NAME: '1UTPOS',
            REQUEST_TYPE: 'LIST.HEAD_DATA.STORED',
            LOV_ALIAS_NAME_1: '',
            LOV_ALIAS_VALUE_1: '',
            LOV_ALIAS_TYPE_1: '',
        },
        async: false,
        method: 'POST',
    });
    var gridresultTipo = responseTPOS.responseData.pageData.grid.GRIDRESULT.GRID;
    return gridresultTipo;
}

function gridOS() {
    responseOS = EAM.Ajax.request({
        url: 'GRIDDATA',
        params: {
            GRID_NAME: '1UORDE',
            REQUEST_TYPE: 'LIST.HEAD_DATA.STORED',
            LOV_ALIAS_NAME_1: 1,
            LOV_ALIAS_VALUE_1: 1,
            LOV_ALIAS_TYPE_1: '',
        },
        async: false,
        method: 'POST',
    });
    // var gridresultOS = responseOS.responseData.pageData.grid.GRIDRESULT.GRID;
    var gridresultOS = responseOS.responseData.pageData.grid.GRIDRESULT.GRID.DATA;
    return gridresultOS;
}

// function pegarOSconsole() {
//     const osTEST = gridOS();
//     console.log("osTST", osTEST.DATA);
//     }

function eventoClickMenu() {
    let listaOpcoesMenu = document.getElementsByClassName('menu');

    for (let index = 0; index < listaOpcoesMenu.length; index++) {
        listaOpcoesMenu[index].addEventListener(
            'click',
            function () {
                let depart = listaOpcoesMenu[index].innerHTML;
                selectDepartment(depart);
            },
            false
        );
    }
}

function selectDepartment(department) {
    let menuBotao = document.getElementById('dropdown-btn');
    menuBotao.textContent = department;
}

function eventoClickMenuOs() {
    let listaOpcoesMenuOs = document.getElementsByClassName('menu-os');

    for (let index = 0; index < listaOpcoesMenuOs.length; index++) {
        listaOpcoesMenuOs[index].addEventListener(
            'click',
            function () {
                let os = listaOpcoesMenuOs[index].innerHTML;
                selectOs(os);
            },
            false
        );
    }
}

function selectOs(Os) {
    let menuBotaoOs = document.getElementById('dropdown-btn-os');
    menuBotaoOs.textContent = Os;
}


let dadosMes;
let mesFormatado;

function configuracoesDeTela() {
    try {
        Ext.ComponentQuery.query('uxtabpanel')[0].el.dom.style.height = '0px';
        var referencenode =
            Ext.ComponentQuery.query('uxtabpanel')[0].up().body.dom;
        vWidth = referencenode.clientWidth;
        vHeigh = referencenode.clientHeight;
        Ext.ComponentQuery.query('[uftId=resetrec]')[0].enable();
        Ext.ComponentQuery.query('[uftId=enterdesigner]')[0].disable();
        Ext.ComponentQuery.query('[uftId=epak]')[0].disable();
        Ext.ComponentQuery.query('[uftId=help]')[0].disable();
    } catch (err) {
        console.log(err);
    }

    // remove custom-div
    var vCustomDiv = document.getElementById('custom-div');
    if (vCustomDiv) {
        vCustomDiv.parentElement.removeChild(vCustomDiv);
    }

    // create custom-div
    var node = document.createElement('div');
    node.id = 'custom-div';
    node.style.width = '100%';
    node.style.height = '100%';
    referencenode.appendChild(node);

    //add html page
    var divcustom = document.getElementById('custom-div');
    divcustom.innerHTML = htmlPage;
}

function gerarDadosMes(valor = null) {
    let dataAtual = new Date();
    let mes;
    let ano;

    if (valor == null) {
        mes = dataAtual.getMonth() + 1;
        ano = dataAtual.getFullYear();
    }

    if (valor == 'posterior') {
        if (dadosMes.mes == 12) {
            mes = 1;
            ano = dadosMes.ano + 1;
        } else {
            mes = dadosMes.mes + 1;
            ano = dadosMes.ano;
        }
    }

    if (valor == 'anterior') {
        if (dadosMes.mes == 1) {
            mes = 12;
            ano = dadosMes.ano - 1;
        } else {
            mes = dadosMes.mes - 1;
            ano = dadosMes.ano;
        }
    }

    let data = new Date(ano, mes, 0);
    let diasNoMes = data.getDate();

    let mesString =
        mes == 1
            ? 'Janeiro'
            : mes == 2
            ? 'Fevereiro'
            : mes == 3
            ? 'Março'
            : mes == 4
            ? 'Abril'
            : mes == 5
            ? 'Maio'
            : mes == 6
            ? 'Junho'
            : mes == 7
            ? 'Julho'
            : mes == 8
            ? 'Agosto'
            : mes == 9
            ? 'Setembro'
            : mes == 10
            ? 'Outubro'
            : mes == 11
            ? 'Novembro'
            : mes == 12
            ? 'Dezembro'
            : 'error';

    dadosMes = {
        dias: diasNoMes,
        mes: mes,
        ano: ano,
    };

    mesFormatado = formataMes(mes);

    let mesNomeContainer = document.getElementById('mesNomeContainer');
    mesNomeContainer.innerHTML = `<h1 class="tituloMes">${mesString} / ${ano}</h1>`;

    let mesNomeTop = document.getElementById('mesTop');
    mesNomeTop.innerHTML = `<p class= tituloMesTop>${mesString}/${ano}</p>`;
}

function verificaEventosBotaoTrocaMes() {
    document
        .getElementById('mesAnterior')
        .addEventListener('click', function () {
            gerarDadosMes('anterior');
            geraCadendarioMes();
            geraOS();
            atualizaHorasTotaisPorDia()
        });
    document
        .getElementById('mesPosterior')
        .addEventListener('click', function () {
            gerarDadosMes('posterior');
            geraCadendarioMes();
            geraOS();
            atualizaHorasTotaisPorDia()
            
        });
}

function geraCadendarioMes() {
    var sectionMes1 = document.getElementsByClassName('mesLinha1')[0];
    var sectionMes2 = document.getElementsByClassName('mesLinha2')[0];
    sectionMes1.innerHTML = '<div></div>';
    sectionMes2.innerHTML = '<div></div>';

    let mesLinha1 = [];
    let mesLinha2 = [];

    for (let dia = 1; dia < dadosMes.dias + 1; dia++) {
        let diaFormatado = formataDia(dia);
        var diaComponent = [
            `<div class="dayContainer">`,
            `    <div id="divHorasTotaisOsDia-${diaFormatado}">`,
            `    <span id="horasTotaisOsDia-${diaFormatado}">0h</span>`,
            `    </div>`,
            `    <div class="numberContainer">`,
            `        <p>${dia}</p>`,
            `    </div>`,
            `    <div `,
            `        class="areaContainer" `,
            `        id="${diaFormatado}-${mesFormatado}-${dadosMes.ano}"`,
            `    >`,
            `    </div>`,
            `</div>`,
        ].join('');

        if (dia <= 31) {
            mesLinha1.push(diaComponent);
        } else {
            mesLinha2.push(diaComponent);
        }
    }
    let mesHtml1 = mesLinha1.join('');
    let mesHtml2 = mesLinha2.join('');
    sectionMes1.innerHTML = mesHtml1;
    sectionMes2.innerHTML = mesHtml2;
    adicionaDropDragOver(dadosMes.dias);
}

function adicionaDropDragOver(dias) {
    for (let dia = 1; dia < dias + 1; dia++) {
        let diaFormatado = formataDia(dia);
            document
            .getElementById(`${diaFormatado}-${mesFormatado}-${dadosMes.ano}`)
            .addEventListener('drop', function (event) {
                event.preventDefault();
                const data = event.dataTransfer.getData('Text');
                if (event.target.className == 'areaContainer') {
                    event.target.style.backgroundColor = 'beige';
                    event.target.style.border = '';
                    let component = document.getElementById(data);
                    try {
                        event.target.appendChild(component);
                    } catch (error) {
                        console.log(error)
                    }                        
                }
            });
        document
            .getElementById(`${diaFormatado}-${mesFormatado}-${dadosMes.ano}`)
            .addEventListener('dragover', function (event) {
                event.preventDefault();
            });
    }
        document.addEventListener('dragenter', function (event) {
            if (event.target.className == 'areaContainer') {
                event.target.style.backgroundColor = '#d6d6b8';
                event.target.style.border = '1px dashed black';
            }
        });
        document.addEventListener('dragleave', function (event) {
            if (event.target.className == 'areaContainer') {
                event.target.style.backgroundColor = 'beige';
                event.target.style.border = '';
            }
        });
}

let osInfos = pegarOS();

function geraOS() {
    let areaDia;

    for (let iOs = 0; iOs < osInfos.length; iOs++) {
        let dateObj = new Date(osInfos[iOs].data);
        let dia = dateObj.getDate().toString().length == 1 ? `0${dateObj.getDate()}` : dateObj.getDate();
        let mes = (dateObj.getMonth() + 1).toString().length == 1 ? `0${(dateObj.getMonth() + 1)}` : (dateObj.getMonth() + 1);
        let ano = dateObj.getFullYear();

        if(mesFormatado == mes) {
            areaDia = document.getElementById(`${dia}-${mes}-${ano}`);
            let osExistente = areaDia.innerHTML
            
            var os = [
                `<div `,
                `    id="${osInfos[iOs].os}"`,
                `    class="osContainer"`,
                `    draggable="true"`,
                `    data-horas="${osInfos[iOs].horas}"`,
                `>`,
                `    <p>${osInfos[iOs].os}</p>`,
                `</div>`,
            ].join('');       
    
            areaDia.innerHTML = osExistente + os;
        }
    }   

    adicionaDragStartDragEnd(osInfos);
}

function atualizaHorasTotaisPorDia() {
    for (let dia = 1; dia < dadosMes.dias + 1; dia++) {
        let diaFormatado = formataDia(dia);
        let areaDia = document.getElementById(`${diaFormatado}-${mesFormatado}-${dadosMes.ano}`);
        let osContainers = areaDia.getElementsByClassName('osContainer');
        let osDia = areaDia.children
        let totalHoras = 0;

        for (let os of osContainers) {
            totalHoras += parseInt(os.getAttribute('data-horas'));    
        }

        let horasTotaisElement = document.getElementById(`horasTotaisOsDia-${diaFormatado}`);
        horasTotaisElement.textContent = `${totalHoras}h`;
        
        var div = document.querySelector('.osContainer');

        if(totalHoras >= 37) {
            //RED
            for (let index = 0; index < osDia.length; index++) {
                osDia[index].style.backgroundColor = '#e9615c';
                osDia[index].style.borderColor = '#a2413e';     
            }       
        }

        else if(totalHoras >= 32  && totalHoras <= 36  ){
            //YELL
            for (let index = 0; index < osDia.length; index++) {
                osDia[index].style.backgroundColor = '#FFBF2E';
                osDia[index].style.borderColor = '#b1841a';     
            }   
        }

        else{
            //GREEN
            for (let index = 0; index < osDia.length; index++) {
                osDia[index].style.backgroundColor = '#93d358';
                osDia[index].style.borderColor = '#73a744';
            } 
        }
    }
}

// function totalHorasMes(){


function adicionaDragStartDragEnd(idList) {
    for (let id = 0; id < idList.length; id++) {
        let mes = idList[id].data.getMonth().toString().length == 1 ? `0${idList[id].data.getMonth()}` : idList[id].data.getMonth();
        
        if(mesFormatado == mes) {
            document
                .getElementById(`${idList[id].os}`)
                .addEventListener('dragstart', function (event) {
                    event.dataTransfer.setData('Text', event.target.id);
                    event.target.style.opacity = '0.4';
                });
            document
                .getElementById(`${idList[id].os}`)
                .addEventListener('dragend', function (event) {
                    event.target.style.opacity = '1';
                    atualizaHorasTotaisPorDia();
                });
        }
    }
}

function formataMes(mes) {
    let mesString = mes.toString();
    let mesFormatado = mesString.length == 1 ? `0${mesString}` : mesString;
    return mesFormatado;
}

function formataDia(dia) {
    let diaString = dia.toString();
    let diaFormatado = diaString.length == 1 ? `0${diaString}` : diaString;
    return diaFormatado;
}

function pegarDepartamentos() {
    const depart = gridDepart();
  //console.log('Departamentos', depart.DATA);
    const divDepartamentos = document.getElementById('dropdown-content-id');
    divDepartamentos.innerHTML = '';
    depart.DATA.forEach(item => {
        const addTag = document.createElement('a');
        addTag.setAttribute('href', '#');
        addTag.setAttribute('class', 'menu');
        addTag.textContent = item.mrc_code;
        divDepartamentos.appendChild(addTag);
    });
    eventoClickMenu();
}

function pegarTipo() {
    const Tipo = gridTipoOS();
  //console.log("TipoOS", Tipo.DATA);
    const divTipoOS = document.getElementById("dropdown-content-tipo");
    divTipoOS.innerHTML = "";
    Tipo.DATA.forEach((item) => {
        const addTag = document.createElement("a");
        addTag.setAttribute("href", "#");
        addTag.setAttribute("class", "menu-os");
        addTag.textContent = item.uco_desc;
        divTipoOS.appendChild(addTag);
    });
    eventoClickMenuOs();
    }

    function pegarOS() {
        const os = gridOS();
        let listaDeOS = [];
        for (let index = 0; index < os.length; index++) {
            let numeroAleatiorio = Math.floor(Math.random() * 10);
            let numeroAleatiorioSemZero = numeroAleatiorio == 0 ? 1 : numeroAleatiorio;
            listaDeOS.push({
                os: os[index].evt_code,
                horas: numeroAleatiorioSemZero,
                data: new Date(os[index].act_start)
                // data: new Date("2024", numeroAleatiorioSemZero % 2 == 0 ? "5" : "6", numeroAleatiorioSemZero.toString())
            })   
        }
        return listaDeOS;
    }

function barraRolagem() {
    const customDiv = document.getElementById('custom-div');

    if (customDiv) {    
        customDiv.style.overflow = 'auto';
        customDiv.style.height = '98%';
    }
}

Ext.define('EAM.custom.external_devcal', {
    extend: 'EAM.custom.AbstractExtensibleFramework',
    getSelectors: function () {
        return {
            '[extensibleFramework] [tabName=HDR][isTabView=true]': {
                afterRender: function () {
                    console.log('Teste: afterrender');
                },
                afterloaddata: function () {
                    console.log('Teste: afterload');    
                },
                afterlayout: function () {
                    try {
                        if (
                            EAM.Utils.getScreen().userFunction == 'DEVCAL' &&
                            Ext.ComponentQuery.query('uxtabpanel')[0]
                        ) {
                            configuracoesDeTela();
                            verificaEventosBotaoTrocaMes();
                            gerarDadosMes();
                            eventoClickMenu();
                            eventoClickMenuOs();
                            geraCadendarioMes();
                            geraOS();
                            pegarDepartamentos();
                            geraCadendarioMes();
                            geraOS();
                            atualizaHorasTotaisPorDia();
                            pegarTipo();
                            pegarOS();
                            // pegarOSconsole();
                            barraRolagem();
                            console.log('Teste');
                        }
                    } catch (err) {
                        console.log(err);
                    }
                },
            },
        };
    },
});
